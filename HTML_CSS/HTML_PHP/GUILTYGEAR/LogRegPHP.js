$.ajax({
    type:'POST',
    url: 'SessionSiempre.php',
    dataType:'json',
    success:function (data){
        if (data!==1){
            $("#Log").html(data);
        }
        window.stop();
    },
    error: function (a, b, c) {
        console.log(a);
        window.stop();
    }
});



$("#RegisterObject").on("click", function (event){
    event.preventDefault();
    if (false){
        alert("L'usuari a de contenir @ies-sabadell.cat en el nom");
    } else {

        $.ajax({
            type: "POST",
            url: 'Register.php',
            data: {
                'userR': $("#RegisterUser").val(),
                'alias': $("#RegisterAlias").val(),
                'pwd': $("#RegisterPassword").val()
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data === 1) {
                    alert('Aquest nom ja existeix')
                } else if (data === 3) {
                    alert("L usuari no conté @ies-sabadell.cat en el nom ")
                }
                else {
                    alert('Usuari Registrat')
                }
                window.stop();
            },
            error: function (a, b, c) {
                console.log(a);
                window.stop();
            }
        });
    }
});
$('#LoginObject').on('click', function (event){
    event.preventDefault();
    $.ajax({
        type:"POST",
        url:"LogIn.php",
        data:{'userL':$('#LoginUserName').val(), 'pw':$('#LoginPassword').val()},
        dataType: 'json',
        success:function (data){
            if (data===1){
                alert("Usuario logeado");
                $('#Log').html($('#LoginUserName').val());
            }
            else{
                console.log(data)
                alert('No se ha podido logear');
            }
            window.stop();
        },
        error: function (a, b, c) {
            console.log(a);
            window.stop();
        }
    })
})
$('#LogOut').on('click', function (){
    $.ajax({
        type:'POST',
        url:'LogOut.php'    ,
        success:function (data){
            $('#Log').html('');
            window.stop();
        },
        error: function (a, b, c) {
            console.log(a);
            window.stop();
        }
    })
})