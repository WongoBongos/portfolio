$(".dropdown-item").on('click',function (){
    if ($(this).text()== 'Guilty Gear'){
        $("body").removeClass("bg-primary").addClass("bg-danger")
$("nav").removeClass("navbar-light").addClass("bg-black").addClass("navbar-dark")
        $(".B").removeClass("btn-outline-dark").removeClass("btn-outline-light").addClass("btn-outline-danger")
        $("#myAlert").removeClass("alert-danger").addClass("alert-primary")
        $(".a").removeClass("btn-primary").addClass("btn-danger")
        $("table").removeClass("table-primary").removeClass("table-danger").addClass("table-dark")
        $("li").removeClass("text-white").addClass("text-dark")
        $("h1").removeClass("text-white").addClass("text-dark")
        $("h2").removeClass("text-white").addClass("text-dark")
        $("h3").removeClass("text-white").addClass("text-dark")



    }
     if ($(this).text()== 'Team Red'){
         $("body").removeClass("bg-primary").addClass("bg-danger")
     $("nav").removeClass("bg-black").removeClass("navbar-dark").addClass("navbar-light").removeClass("bg-primary")
         $(".B").removeClass("btn-outline-danger").removeClass("btn-outline-primary").addClass("btn-outline-dark")
         $("#myAlert").removeClass("alert-primary").addClass("alert-danger")
         $(".a").removeClass("btn-primary").addClass("btn-danger")
         $("table").removeClass("table-dark").removeClass("table-primary").addClass("table-danger")
         $("p").removeClass("text-white").addClass("text-dark")
         $("li").removeClass("text-white").addClass("text-dark")
         $("h1").removeClass("text-white").addClass("text-dark")
         $("h2").removeClass("text-white").addClass("text-dark")
         $("h3").removeClass("text-white").addClass("text-dark")

    }
     if ($(this).text()== 'Team Blue'){
         $("body").removeClass("bg-danger").addClass("bg-primary")
         $("nav").removeClass("bg-black").removeClass("navbar-light").addClass("bg-primary").addClass("navbar-dark")
         $(".B").removeClass("btn-outline-danger").removeClass("btn-outline-dark").addClass("btn-outline-light")
         $("#myAlert").removeClass("alert-danger").addClass("alert-primary")
         $(".a").removeClass("btn-danger").addClass("btn-primary")
         $("table").removeClass("table-dark").removeClass("table-danger").addClass("table-primary")
         $("p").removeClass("text-black").addClass("text-white")
         $("li").removeClass("text-dark").addClass("text-white")
         $("h1").removeClass("text-dark").addClass("text-white")
         $("h2").removeClass("text-dark").addClass("text-white")
         $("h3").removeClass("text-dark").addClass("text-white")
    }
})