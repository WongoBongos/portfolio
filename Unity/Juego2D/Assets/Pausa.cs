using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausa : MonoBehaviour
{
    [SerializeField]
    private GameObject reanudar;
    [SerializeField]
    private GameObject rendirse;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pausar() { 
        reanudar.SetActive(true);
        rendirse.SetActive(true);
        Time.timeScale = 0f;
    }

    public void despausar() {
        reanudar.SetActive(false);
        rendirse.SetActive(false);
        Time.timeScale = 1f;
    }

    public void giveUp() {
        SceneManager.LoadScene("Inicio");
    }


}
