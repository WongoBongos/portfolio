using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy/Enemy")]

public class EnemyScriptable : ScriptableObject
{
    public string nombre;
    public float puntos;
    public float vida;
    public float da�o;
    public float velocidad;
    public string Anim1;
    public string Anim2;
    
}
