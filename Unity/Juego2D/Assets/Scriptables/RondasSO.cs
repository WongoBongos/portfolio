using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Rondas", menuName = "Rondas")]
public class RondasSO : ScriptableObject
{
    public int numRondas;
    public int numEnemigos;
}
