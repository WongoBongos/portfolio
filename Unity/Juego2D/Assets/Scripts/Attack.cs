using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{

    [SerializeField]
    private PlayerSO ps;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [SerializeField]
    private LayerMask layerMask;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy") {
        
            Collider2D collider = collision.GetComponent<Collider2D>();
            EnemyScript enemy = collider.GetComponent<EnemyScript>();
            enemy.QuitarVida(ps);


        }
    }
}
