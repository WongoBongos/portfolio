using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flecha : MonoBehaviour
{
    private Rigidbody2D rb;
    private Vector2 dir;
    [SerializeField]
    private EnemyScriptable enemyScriptable;
    public float �ado { set => �ado = value; }
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * 5f;
        StartCoroutine(autoDestruccion());
    }


    private int cont = 0;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 d = GetComponent<Rigidbody2D>().velocity;
        ContactPoint2D n = collision.GetContact(0);
        dir = Vector2.Reflect(d, n.normal).normalized;
        transform.up = dir;
        rb.velocity = transform.up * 3f;
        cont++;
        if (cont == 3) {
            Destroy(gameObject);
        }
        if (collision.transform.tag == "player") {
            GameObject go = collision.gameObject;
            PlayerScript ps = go.GetComponent<PlayerScript>();
            ps.quitarVida(enemyScriptable);
            Destroy(gameObject);
        }
        
    }

    IEnumerator autoDestruccion() {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);
    }

    
}
