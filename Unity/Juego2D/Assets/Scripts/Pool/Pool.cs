using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{

    [SerializeField]
    private GameObject[] m_PoolableElements;
    [SerializeField]
    private int m_Size;
    public int size { set=>m_Size=value; get => m_Size; }

    private List<GameObject> m_Pool;
    public List<GameObject> pool { get => m_Pool; }

    public void rellenar() {
            for (int i = 0; i < m_PoolableElements.Length; i++)
            {
                if (!m_PoolableElements[i].GetComponent<Poolable>())
                {
                    Debug.LogError(gameObject + ": Poolable element without a Poolable component");
                    Destroy(this);
                }
            }


            m_Pool = new List<GameObject>();
            for (int i = 0; i < m_Size; ++i)
            {
                GameObject element = Instantiate(m_PoolableElements[Random.Range(0, m_PoolableElements.Length)], transform);
                element.GetComponent<Poolable>().SetPool(this);
                element.SetActive(false);
                m_Pool.Add(element);
            }
        
    }

    public void vaciar() {
        for (int i = 0; i < m_Size; i++) {
            Destroy(m_Pool[i]);
        }
    }
    public bool ReturnElement(GameObject element)
    {
        if (m_Pool.Contains(element))
        {
            element.SetActive(false);
            return true;
        }

        return false;
    }

    public GameObject GetElement()
    {
        foreach (GameObject element in m_Pool)
        {
            if (!element.activeInHierarchy)
            {
                element.SetActive(true);
                return element;
            }
        }

        return null;
    }


}
