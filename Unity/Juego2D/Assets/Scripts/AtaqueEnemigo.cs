using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour
{
    [SerializeField]
    private EnemyScriptable enemyScriptable;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "player") {
            GameObject gameObject = collision.gameObject;
            PlayerScript ps = gameObject.GetComponent<PlayerScript>();
            ps.quitarVida(enemyScriptable);
        }
    }
}
