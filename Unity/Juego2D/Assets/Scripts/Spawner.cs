using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    private bool inactivos;
    [SerializeField]
    private Transform player;
    [SerializeField]
    private float SpawnRate = 3f;

    [SerializeField]
    private RondasSO rso;
    [SerializeField]
    private Transform[] Spawnpoints;

    [SerializeField]
    private PuntosEvent pe;
    [SerializeField]
    private GameObject flecha;

    public delegate void DeshabilitarInputs();
    public DeshabilitarInputs onSceneChange;

    [SerializeField]
    private Pool pool;
    private float contador;
    void Start()
    {
        pool.size = rso.numRondas;
        pool.rellenar();
        player.gameObject.GetComponent<PlayerScript>().gameOver += gameOver;
        StartCoroutine(spawnear());
        StartCoroutine(rondas());
    }

    // Update is called once per frame
    void Update()
    {
      
    }
    
    IEnumerator spawnear()
    {
        while (pool.size != contador)
        {
            GameObject g = pool.GetElement();

            EnemyScript enemy = g.GetComponent<EnemyScript>();
            if (enemy)
            {
                enemy.objetivo = player;
                enemy.projectil = flecha;
                enemy.puntosEvent = pe;
            }
            g.transform.position = Spawnpoints[Random.Range(0, Spawnpoints.Length)].position;
            contador += 1;
            yield return new WaitForSeconds(SpawnRate);
        }
    }

    IEnumerator rondas()
    {
        while (true) {
            checkinactivos();
            if (pool.size == contador && inactivos)
            {
                StopCoroutine(spawnear());
                pool.vaciar();
                pool.size = pool.size + 1;
                contador = 0;
                pool.rellenar();
                yield return new WaitForSeconds(5f);
                rso.numRondas += 1;
                if (rso.numRondas == 5) {
                    CambiarMapa();
                }
                StartCoroutine(spawnear());
            }
            yield return null;
        }
        
    }

    private void checkinactivos() { 
       inactivos = true;
        for (int i = 0; i < pool.size; i++) {

            if (pool.pool[i].activeInHierarchy)
                inactivos = false;
            
            
        }
    }

    private void CambiarMapa() {
        StopAllCoroutines();
        pool.vaciar();
        onSceneChange.Invoke();
        SceneManager.LoadScene("Nivel2");
        StartCoroutine(spawnear());
        StartCoroutine(rondas());
    }

    public void gameOver() {
        StopAllCoroutines();
        pool.vaciar();
  
    }
}

