using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using static UnityEditor.VersionControl.Asset;

public class EnemyScript : MonoBehaviour
{
    private enum States { RUN, ATTACK }
    private States m_CurrentState;

    private Rigidbody2D m_Rigidbody;
    [SerializeField]
    private EnemyAttack ea;

    private PuntosEvent pe;
    public PuntosEvent puntosEvent { set => pe = value; }

    [SerializeField]
    private EnemyScriptable es;

    [SerializeField]
    private float vida;
    
    private GameObject bala;
    public GameObject projectil { set => bala = value; }
  
    [SerializeField]
    private Transform targetPos;
    public Transform objetivo { 
        set=>targetPos = value;
    }

    private Animator animator;
    private bool atacar = false;
    private bool tepersigo = true;



    void Start()
    {
        vida = es.vida;  
        m_Rigidbody = GetComponent<Rigidbody2D>();  
        ea.personaDetectado += Atacar;
        ea.sefue += DejarAtacar;
        m_CurrentState = States.RUN;
        animator = GetComponent<Animator>();
        InitState(m_CurrentState);

    }

    // Update is called once per frame
    void Update()
    {
        UpdateState(m_CurrentState);
        if(tepersigo)
        transform.position = Vector2.MoveTowards(transform.position, targetPos.position, es.velocidad * Time.deltaTime);


        if (transform.position.x > targetPos.position.x) {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else if (transform.position.x < targetPos.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }


    public void QuitarVida(PlayerSO ps) {
        vida = vida - ps.ataque;
        if (vida <= 0)
        {
            pe.Raise(es);
            GetComponent<Poolable>().ReturnToPool();
        }
     
    }

    private float m_StateDeltaTime;
    private Coroutine corrutinaperseguir;

    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;
        m_StateDeltaTime = 0;

        switch (m_CurrentState)
        {
            case States.RUN:
                   animator.Play(es.Anim1);
                tepersigo = true; 
          
                break;
            case States.ATTACK:
                animator.Play(es.Anim2);
                break;
            default:
                break;
        }
    }
    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.RUN:
                if (atacar)
                    ChangeState(States.ATTACK);
          
                break;
               case States.ATTACK:
                if (!atacar || m_StateDeltaTime >= 1.10f)
                    ChangeState(States.RUN);
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.RUN:
                tepersigo = false;
                break;
            case States.ATTACK:
           
                break;
        
            default:
                break;
        }
    }

    private void Atacar() {
        atacar = true;

    }
    private void DejarAtacar()
    {
        atacar = false;

    }

    private void Shoot() {
        
            GameObject arrow = Instantiate(bala);
            arrow.transform.position = transform.position;
            Vector2 dir = targetPos.position - transform.position;
            arrow.transform.up = dir;

        
    }
}
