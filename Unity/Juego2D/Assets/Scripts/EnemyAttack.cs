using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public delegate void PersonaDetectado();
    public PersonaDetectado personaDetectado;
    public delegate void PersonaAlejado();
    public PersonaAlejado sefue;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "player") {
            personaDetectado.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
       
        if (collision.transform.tag == "player")
        {
          
            sefue.Invoke();
        }
    }
}
