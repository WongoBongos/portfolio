using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OleadaScript : MonoBehaviour
{
    [SerializeField]
    private RondasSO rso;
    private TMPro.TextMeshProUGUI m_Text;
    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Text.text = "Oleada: " + rso.numRondas;
    }
}
