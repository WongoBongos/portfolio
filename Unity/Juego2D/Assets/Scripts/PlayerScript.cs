using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using static UnityEditor.VersionControl.Asset;

public class PlayerScript : MonoBehaviour
{
    private enum States { IDLE, RUN, ATTACK1, ATTACK2, ATTACK3 }
    private States m_CurrentState;
    [SerializeField]
    private PlayerSO ps;

    [SerializeField]
    private Spawner spawner;

    public delegate void PlayerGameOver();
    public PlayerGameOver gameOver;
    private Rigidbody2D rb;
    private PlayerInputs pi;
    [SerializeField]
    private float m_Speed = 3;
    private Vector2 movement;
    private Animator animator;
    bool combo = false;
    void Start()
    {
        spawner.onSceneChange += disable;
        m_CurrentState = States.IDLE;
        animator = GetComponent<Animator>();    
        rb = GetComponent<Rigidbody2D>();
        pi = new PlayerInputs();
        pi.PlayerMovement.Movimiento.canceled += StopMovement;
        pi.Enable();

    }
    // Update is called once per frame
    void Update()
    {
        UpdateState(m_CurrentState);
        movement = pi.PlayerMovement.Movimiento.ReadValue<Vector2>();
        if (movement.x > 0) {
            rb.velocity = new Vector2(m_Speed, rb.velocity.y);
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if(movement.x < 0)
        {
            rb.velocity = new Vector2(-m_Speed, rb.velocity.y);
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        if (movement.y > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, m_Speed);
        }
        else if (movement.y < 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, -m_Speed);
        }
    }

    private void StopMovement(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        if (context.phase == InputActionPhase.Canceled)
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
    private IEnumerator CorutineCombo(float comboWindowStart, float comboWindowEnd)
    {
        combo = false;
        yield return new WaitForSeconds(comboWindowStart);
        combo = true;
        yield return new WaitForSeconds(comboWindowEnd);
        combo = false;
    }

    private float m_StateDeltaTime;
    private Coroutine m_ComboTimeCoroutine;

    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;
        m_StateDeltaTime = 0;

        switch (m_CurrentState)
        {
            case States.IDLE:
                animator.Play("idle");
                break;
            case States.RUN:
                animator.Play("run");
                break;
            case States.ATTACK1:
                animator.Play("attack");
                m_ComboTimeCoroutine = StartCoroutine(CorutineCombo(0.11f, 0.3f));
                break;
            case States.ATTACK2:
                animator.Play("attack2");
                m_ComboTimeCoroutine = StartCoroutine(CorutineCombo(0.11f, 0.3f));
                break;
            case States.ATTACK3:
                animator.Play("attack3");
                break;
            default:
                break;
        }
    }


    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.IDLE:
                movement = pi.PlayerMovement.Movimiento.ReadValue<Vector2>();
                if (Input.GetKeyDown(KeyCode.Space))
                    ChangeState(States.ATTACK1);
                if (movement.x > 0 || movement.x < 0 || movement.y > 0 || movement.y < 0) {
                    ChangeState(States.RUN);
                }

                break;
            case States.RUN:
                if (Input.GetKeyDown(KeyCode.Space))
                    ChangeState(States.ATTACK1);

                else if (m_StateDeltaTime >= 1.24f)
                    ChangeState(States.IDLE);

                break;
            case States.ATTACK1:
                if (Input.GetKeyDown(KeyCode.Space) && combo)
                    ChangeState(States.ATTACK2);

                else if (m_StateDeltaTime >= 0.3f)
                    ChangeState(States.IDLE);

                break;
            case States.ATTACK2:
                if (Input.GetKeyDown(KeyCode.Space) && combo)
                    ChangeState(States.ATTACK3);

                else if (m_StateDeltaTime >= 0.3f)
                    ChangeState(States.IDLE);

                break;
            case States.ATTACK3:
                if (m_StateDeltaTime >= 1.10f)
                    ChangeState(States.IDLE);

                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.IDLE:
                break;
            case States.RUN:
                break;
            case States.ATTACK1:
                StopCoroutine(m_ComboTimeCoroutine);
                break;
            case States.ATTACK2:
                StopCoroutine(m_ComboTimeCoroutine);
                break;
            case States.ATTACK3:
                break;
            default:
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "lava") {
            ps.vida = ps.vida - 1;
            if (ps.vida <= 0)
            {
                gameOver.Invoke();
                pi.Disable();
                Destroy(gameObject);
                SceneManager.LoadScene("GameOver");
            }
        }
    }

    
    public void actualizarPuntos(EnemyScriptable es) {
        ps.puntos += es.puntos;
    }

    public void quitarVida(EnemyScriptable es) { 
        ps.vida = ps.vida - es.da�o;
        if (ps.vida <= 0) {
            gameOver.Invoke();
            pi.Disable();
            Destroy(gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }

    private void disable()
    {
        pi.Disable();
    }
}
