using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/PuntosEvent")]
public class PuntosEvent : GameEvent<EnemyScriptable>{}
