using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesInicio : MonoBehaviour
{
    [SerializeField]
    private RondasSO rso;
    [SerializeField]
    private PlayerSO pso;
    void Awake()
    {
        rso.numRondas = 1;
        rso.numEnemigos = 0;
        pso.vida = pso.vidaMax; 
        pso.puntos = 0;
        
    }


}
