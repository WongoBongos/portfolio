using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ganar : MonoBehaviour
{
    [SerializeField]
    private CambioDeEscenaEvent c;
    private void OnTriggerEnter(Collider other)
    {
     
        if (other.transform.tag == "player")
        {
            c.Raise();
            SceneManager.LoadScene("Ganaste");
        }
    }
}
