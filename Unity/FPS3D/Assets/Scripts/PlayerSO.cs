using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerSO", menuName = "PlayerSO")]
public class PlayerSO : ScriptableObject
{
    public int vidaMax;
    public int vida;
    public int score;
}
