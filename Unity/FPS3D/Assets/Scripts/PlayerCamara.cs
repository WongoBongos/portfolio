using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamara : MonoBehaviour
{

    private PlayerInputs pi;
    private Vector2 movimiento;

    private float xRotation = 0f;

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private Ganar g;
    void Start()
    {
   
        pi = new PlayerInputs();
        pi.Enable();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {

        movimiento = pi.Camara.Look.ReadValue<Vector2>();

        float mouseX = movimiento.y * 100f * Time.deltaTime;
        float mousey = movimiento.x * 100f * Time.deltaTime;

        xRotation -= mouseX;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localEulerAngles = new Vector3(xRotation, 0, 0);

        player.transform.Rotate(Vector3.up * mousey);
        


    }
    public void desactivarLock() { 
        Cursor.lockState = CursorLockMode.None;
    }
}
