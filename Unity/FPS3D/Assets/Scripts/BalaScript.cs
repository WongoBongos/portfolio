using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaScript : MonoBehaviour
{   
    private Rigidbody rb;
    [SerializeField]
    private PlayerHaceDa�oEvent onEnemyHit;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "enemy")
        {
            Debug.Log("MAto");
            other.gameObject.GetComponent<EnemyScript>().quitarVida();
            this.GetComponent<Poolable>().ReturnToPool();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        List<ContactPoint> contacts = new List<ContactPoint>();
        collision.GetContacts(contacts);
        ContactPoint contacte = collision.GetContact(0);
        Vector3 nouForward = Vector3.Reflect(transform.forward, contacte.normal);
        transform.forward = nouForward;
        rb.velocity = transform.forward * 5f;

    }

}
