using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private PlayerInputs pi;
    private Rigidbody rb;

    private Vector3 movimiento;

    float m_MoveSpeed = 10f;
    [SerializeField]
    private Pool arma;
    [SerializeField]
    private Transform mira;
    [SerializeField]
    private PlayerSO ps;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pi = new PlayerInputs();
        pi.Arma.Disparar.started += Shoot;
        pi.Enable();
        arma.rellenar();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 input = pi.Movimiento.Movement.ReadValue<Vector2>();

        rb.MovePosition(transform.position
            + input.x * transform.right * m_MoveSpeed * Time.deltaTime
            + input.y * transform.forward * m_MoveSpeed * Time.deltaTime);
    }

    float contador = 0;
    private void Shoot(InputAction.CallbackContext context) {
        if (contador != arma.size)
        {
            GameObject bala = arma.GetElement();
            bala.transform.position = mira.transform.position;
            bala.transform.forward = mira.transform.forward;
            bala.GetComponent<Rigidbody>().velocity = bala.transform.forward * 3f;
            contador++;
        }
        else {
            arma.volver();
            contador = 0;
        }

    }

    public void QuitarVida() {
        ps.vida = ps.vida - 1;
        if (ps.vida == 0) {
            Cursor.lockState = CursorLockMode.None;
            pi.Disable();
            SceneManager.LoadScene("GameOver");
        }
    }

    public void GanarPuntos() {
        ps.score += 100;
    }
    public void ApaagarInpust() {
        pi.Disable();
    }
}
