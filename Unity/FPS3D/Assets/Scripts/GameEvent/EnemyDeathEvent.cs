using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/EnemyDeathEvent")]
public class EnemyDeathEvent : GameEvent
{
    
}
