using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/PlayerHaceDañoEvent")]
public class PlayerHaceDañoEvent : GameEvent {}
