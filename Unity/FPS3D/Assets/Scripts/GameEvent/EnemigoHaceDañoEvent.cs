using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/EnemigoHaceDañoEvent")]
public class EnemigoHaceDañoEvent : GameEvent {}
