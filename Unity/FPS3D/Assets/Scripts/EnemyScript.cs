using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{

    [SerializeField]
    private Transform player;
    public Transform playerPos { set => player = value; }
    private NavMeshAgent agent;
    [SerializeField]
    private EnemySO es;
    [SerializeField]
    private EnemigoHaceDañoEvent onPlayerHit;
    private float vida;
    [SerializeField]
    private EnemyDeathEvent onEnemyDeath;
    void Start()
    {
        vida = es.vida;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(player.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "player")
        {
            onPlayerHit.Raise();
            this.GetComponent<Poolable>().ReturnToPool();
        }
    }

    public void quitarVida() {
        vida -= 1;
        Debug.Log("No");
        if (vida <= 0) {
            onEnemyDeath.Raise();
            this.GetComponent<Poolable>().ReturnToPool();
        }
    }




}
