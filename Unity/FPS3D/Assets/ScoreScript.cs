using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreScript : MonoBehaviour
{
    [SerializeField]
    private PlayerSO m_PlayerSO;
    private TMPro.TextMeshProUGUI m_Text;
    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Text.text = "Score: "+m_PlayerSO.score;
    }
}
