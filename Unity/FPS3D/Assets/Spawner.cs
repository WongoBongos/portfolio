using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private Pool pool;
    [SerializeField]
    private Transform[] spawnpoints;
    [SerializeField]
    private Transform player;
    void Start()
    {
        pool.rellenar();
        StartCoroutine(spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawn(){ 
        while(true){
            GameObject go = pool.GetElement();
            EnemyScript es = go.GetComponent<EnemyScript>();
            if (es) {
                es.playerPos = player;
            }
            go.transform.position = spawnpoints[Random.Range(0, spawnpoints.Length)].position;
            yield return new WaitForSeconds(5f);
        }
    }
}
