@extends('layouts.app')

@section('content')
    <div class="container">
          <div class="row">
            <div class="col-xl-12">
                <form method="POST" action="/subhasta/editStore/{{$subhasta["idsubhasta"]}}">
                    <div class="form-group">
                        <label for="tipus">Objecte</label>
                        <select class="form-control " name="objecte" id="objecte">
                            @foreach (\App\Models\objectes::all() as $objecte)
                                <option value="{{ $objecte->idobjecte }}">{{ $objecte->nom }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="licitacio_maxima">Licitació Màxima</label>
                        <input type="text" class="form-control @error('licitacio_maxima') is-invalid @enderror" name="licitacio_maxima" id="licitacio_maxima" value="{{old('licitacio_maxima')}}" required>
                        @error('licitacio_maxima')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="licitacio_minima">Licitació Miníma</label>
                        <input type="text" class="form-control @error('licitacio_minima') is-invalid @enderror" name="licitacio_minima" id="licitacio_minima" value="{{old('licitacio_minima')}}">
                        @error('licitacio_minima')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="data_finalitzacio">Data Finalització</label>
                        <input type="text" class="form-control @error('data_finalitzacio') is-invalid @enderror" name="data_finalitzacio" id="data_finalitzacio" value="{{old('data_finalitzacio')}}">
                        @error('data_finalitzacio')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Desar</button>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
