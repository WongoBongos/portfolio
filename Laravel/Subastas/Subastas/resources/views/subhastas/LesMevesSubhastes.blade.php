@extends('layouts.app')

@section('content')
    <a href="subhasta/add" class="btn btn-primary float-end ">Nova Subhasta</a>
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Jugador</th>
            <th>Objecte</th>
            <th>Licitació Maxíma</th>
            <th>Licitació Miníma</th>
            <th>Licitació Actual</th>
            <th>Activa</th>
            <th>Objecte Subhastat?</th>
            <th>Està el preu pujant?</th>
            <th>Finalització De La Subhasta</th>
            <th>Licitacio Paga Mas</th>
            <th>Editar Subhasta</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subhastes as $subasta)
            @foreach($licitacionsMax as $licitacio)
            <tr>
                <td>{{$subasta['jugador']}}</td>
                <td>{{$subasta['objecte']}}</td>
                <td>{{$subasta['licitacio_maxima']}}</td>
                <td>{{$subasta['licitacio_minima']}}</td>
                <td>{{$subasta['licitacio_actual']}}</td>
                <td>{{$subasta['activa']}}</td>
                <td>{{$subasta['objecte_subhastat']}}</td>
                <td>{{$subasta['preu_puja']}}</td>
                <td>{{$subasta['data_finalitzacio']}}</td>
                <td>{{$licitacio['idlicitacio']}}</td>
                <td><a class="btn btn-primary" href="/subhasta/edit/{{$subasta['idsubhasta']}}">Editar</a></td>
            </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
@endsection
