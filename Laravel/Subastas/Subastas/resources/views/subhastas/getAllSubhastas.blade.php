@extends('layouts.app')

@section('content')
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Jugador</th>
            <th>Objecte</th>
            <th>Licitació Maxíma</th>
            <th>Licitació Miníma</th>
            <th>Licitació Actual</th>
            <th>Activa</th>
            <th>Objecte Subhastat?</th>
            <th>Està el preu pujant?</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subastas as $subasta)
            <tr>
                <td>{{$subasta['jugador']}}</td>
                <td>{{$subasta['objecte']}}</td>
                <td>{{$subasta['licitacio_maxima']}}</td>
                <td>{{$subasta['licitacio_minima']}}</td>
                <td>{{$subasta['licitacio_actual']}}</td>
                <td>{{$subasta['activa']}}</td>
                <td>{{$subasta['objecte_subhastat']}}</td>
                <td>{{$subasta['preu_puja']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
