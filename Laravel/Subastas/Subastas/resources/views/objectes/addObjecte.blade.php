@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <form method="POST" action="/objecte/addStore" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="nom" required>
                    </div>
                    <div class="form-group">
                        <label for="any">Any</label>
                        <input type="text" class="form-control" name="any" id="any">
                    </div>
                    <div class="form-group">
                        <label for="peggi">Peggi</label>
                        <input type="text" class="form-control" name="peggi" id="peggi">
                    </div>
                    <div class="form-group">
                        <label for="fabricant">Fabricant</label>
                        <input type="text" class="form-control" name="fabricant" id="fabricant">
                    </div>
                    <div class="form-group">
                        <label for="tipus">Tipus</label>
                        <select class="form-control" name="tipus" id="tipus">
                            @foreach (\App\Models\tipus_objecte::all() as $tipusObjecte)
                                <option value="{{ $tipusObjecte->idtipus }}">{{ $tipusObjecte->nomtipus }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="imatge">Imatge</label>
                        <input type="file" class="form-control-file" name="imatge" id="imatge" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Desar</button>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
