@extends('layouts.app')

@section('content')
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Imatge</th>
            <th>Any</th>
            <th>Peggi</th>
            <th>Fabricant</th>
            <th>Tipus</th>

        </tr>
        </thead>
        <tbody>
        @foreach($objectes as $objecte)
            <tr>
                <td>{{$objecte['nom']}}</td>
                <td><img src="{{asset($objecte['directori'])}}"></td>
                <td>{{$objecte['any']}}</td>
                <td>{{$objecte['peggi']}}</td>
                <td>{{$objecte['fabricant']}}</td>
                @foreach(\App\Models\tipus_objecte::all() as $tipus)
                    @if($tipus["idtipus"] === $objecte["tipus"])
                        <td>{{$tipus["nomtipus"]}}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
