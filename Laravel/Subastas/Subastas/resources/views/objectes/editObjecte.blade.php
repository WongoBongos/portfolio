@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                    <form method="POST" action="/objecte/editStore/{{$objecte["idobjecte"]}}" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" id="nom" value="{{old('nom')}}" required>
                            @error('nom')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="any">Any</label>
                            <input type="text" class="form-control @error('any') is-invalid @enderror" name="any" id="any" value="{{old('any')}}">
                            @error('any')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="peggi">Peggi</label>
                            <input type="text" class="form-control @error('peggi') is-invalid @enderror" name="peggi" id="peggi" value="{{old('peggi')}}">
                            @error('peggi')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="fabricant">Fabricant</label>
                            <input type="text" class="form-control @error('fabricant') is-invalid @enderror" name="fabricant" id="fabricant" value="{{old('fabricant')}}">
                            @error('fabricant')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tipus">Tipus</label>
                            <select class="form-control " name="tipus" id="tipus">
                                @foreach (\App\Models\tipus_objecte::all() as $tipusObjecte)
                                    <option value="{{ $tipusObjecte->idtipus }}">{{ $tipusObjecte->nomtipus }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="imatge">Imatge</label>
                            <input type="file" class="form-control-file" name="imatge" id="imatge" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Desar</button>
                        {{ csrf_field() }}
                    </form>
            </div>
        </div>
    </div>
@endsection
