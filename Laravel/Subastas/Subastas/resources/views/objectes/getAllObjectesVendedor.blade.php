@extends('layouts.app')

@section('content')
    <a href="objecte/add" class="btn btn-primary float-end ">Nou Objecte</a>
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Imatge</th>
            <th>Any</th>
            <th>Peggi</th>
            <th>Fabricant</th>
            <th>Tipus</th>
            <th>Edita l'objecte</th>

        </tr>
        </thead>
        <tbody>
        @foreach($objectes as $objecte)
            <tr>
                <td>{{$objecte['nom']}}</td>
                <td><center><img src="{{asset($objecte['directori'])}}"></center></td>
                <td>{{$objecte['any']}}</td>
                <td>{{$objecte['peggi']}}</td>
                <td>{{$objecte['fabricant']}}</td>
                @foreach(\App\Models\tipus_objecte::all() as $tipus)
                    @if($tipus["idtipus"] === $objecte["tipus"])
                        <td>{{$tipus["nomtipus"]}}</td>
                    @endif
                @endforeach
                <td><a href="/objecte/edit/{{$objecte["idobjecte"]}}" class="btn btn-primary">Edita l'objecte</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
