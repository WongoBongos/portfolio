@extends('layouts.app')

@section('content')
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Preu Ofert</th>
            <th>User</th>
            <th>Subhasta</th>
        </tr>
        </thead>
        <tbody>
            @foreach($licitachions as $licitacio)
                <tr>
                    <td>{{$licitacio['preuofert']}}</td>
                    <td>{{$licitacio['iduser']}}</td>
                    <td>{{$licitacio['subhasta']}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
