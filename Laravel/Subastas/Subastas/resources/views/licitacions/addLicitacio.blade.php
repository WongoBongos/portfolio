@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <form method="POST" action="/licitacio/addStore/{{$idsubhasta}}">
                    <div class="form-group">
                        <label for="preuofert">preuofert</label>
                        <input type="text" class="form-control" name="preuofert" id="preuofert" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Desar</button>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection
