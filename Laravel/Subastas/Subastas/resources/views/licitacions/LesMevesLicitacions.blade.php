@extends('layouts.app')

@section('content')
    <table class="container estilo table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <th>Preu Ofert</th>
            <th>Comprador</th>
            <th>Subhasta</th>
            <th>Objecte</th>
            <th>Finalització Subhasta</th>
        </tr>
        </thead>
        <tbody>
        @foreach($licitacions as $licitacio)
                <tr>
                    <td>{{$licitacio['preuofert']}}</td>
                    <td>{{$licitacio['iduser']}}</td>
                    <td>{{$licitacio['subhasta']}}</td>
                    @foreach(\App\Models\objectes::all() as $objecte)
                        @if(\App\Models\subhastes::find($licitacio['subhasta'])->objecte == $objecte['idobjecte'])
                            <td>{{$objecte['nom']}}</td>
                        @endif
                    @endforeach
                    <td>{{\App\Models\subhastes::find($licitacio['subhasta'])->data_finalitzacio }}</td>
                </tr>
        @endforeach
        </tbody>
    </table>
@endsection
