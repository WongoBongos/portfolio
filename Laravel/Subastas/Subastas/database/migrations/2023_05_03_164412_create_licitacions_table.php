<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('licitacions', function (Blueprint $table) {
            $table->id("idlicitacio");
            $table->decimal("preuofert");
            $table->foreignId('iduser')->constrained('users')->references('iduser');
            $table->foreignId('subhasta')->constrained('subhastes')->references('idsubhasta');
            $table->timestamp("data_de_licitació")->default(Carbon\Carbon::now());
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licitacions');
    }
}
