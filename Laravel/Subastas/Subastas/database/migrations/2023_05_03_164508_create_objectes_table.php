<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('objectes', function (Blueprint $table) {
            $table->id("idobjecte");
            $table->string("nom");
            $table->string("directori");
            $table->string("any");
            $table->string("peggi");
            $table->string("fabricant");
            $table->foreignId("tipus")->constrained("tipus_objectes")->references("idtipus");
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objectes');
    }
}
