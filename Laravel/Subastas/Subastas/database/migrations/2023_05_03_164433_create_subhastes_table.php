<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubhastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('subhastes', function (Blueprint $table) {
            $table->id("idsubhasta");
            $table->foreignId('jugador')->constrained('users')->references('iduser');
            $table->foreignId('objecte')->constrained('objectes')->references('idobjecte');
            $table->decimal("licitacio_maxima");
            $table->decimal("licitacio_minima");
            $table->decimal("licitacio_actual");
            $table->date("data_finalitzacio");
            $table->boolean("activa");
            $table->boolean("objecte_subhastat");
            $table->boolean("preu_puja");
            $table->foreignId("idplataforma")->constrained("plataformas")->references("idplataforma");
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subhastes');
    }
}
