<?php

namespace Database\Seeders;

use App\Models\Tipus_Objecte;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([RolsSeeder::class]);
        $this->call([user::class]);
        $this->call([TipusObjecteSeeder::class]);
        $this->call([ObjectesSeeder::class]);
        $this->call([PlataformaSeeder::class]);
        $this->call([SubhastesSeeder::class]);
        $this->call([LicitacionsSeeder::class]);


    }
}
