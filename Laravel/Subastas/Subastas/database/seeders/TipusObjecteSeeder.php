<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipusObjecteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Joc de Taula"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Merchandaising"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Consola"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Joc VR"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Accesori"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Joc Retro"
        ]);
        DB::table("tipus_objectes")->insert([
            "nomtipus" => "Joc Actual"
        ]);
    }
}
