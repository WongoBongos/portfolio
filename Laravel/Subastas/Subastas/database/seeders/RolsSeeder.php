<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("rols")->insert([
            'nom' => "Administrador"
        ]);
        DB::table("rols")->insert([
            'nom' => "Venedor"
        ]);
        DB::table("rols")->insert([
            'nom' => "Subhastador"
        ]);
        DB::table("rols")->insert([
            'nom' => "Comprador"
        ]);

    }
}
