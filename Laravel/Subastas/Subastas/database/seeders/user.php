<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table("users")->insert([
         "name"=>"Roberto",
         "email"=>"robertomail@gmail.com",
         "saldo"=>5000,
         "password"=>bcrypt("pinipon"),
         "rol"=> 2
     ]);

        DB::table("users")->insert([
            "name"=>"Mario",
            "email"=>"Mariomail@gmail.com",
            "saldo"=>5000,
            "password"=>bcrypt("Wahoo"),
            "rol"=> 4
        ]);

        DB::table("users")->insert([
            "name"=>"Reyn",
            "email"=>"reyntime@gmail.com",
            "saldo"=>5000,
            "password"=>bcrypt("rainandduncan"),
            "rol"=> 1
        ]);

    }
}
