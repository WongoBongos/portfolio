<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubhastesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("subhastes")->insert([
            "jugador"=>1,
            "objecte"=> 2,
            "licitacio_maxima" => 2000,
            "licitacio_minima" => 10,
            "licitacio_actual" => 2000,
            "data_finalitzacio" => "2023-05-16",
            "activa"=>1,
            "objecte_subhastat"=>0,
            "preu_puja"=>0,
            "idplataforma" => 1
        ]);
    }
}
