<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObjectesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("objectes")->insert([
            "nom"=>"God Of War(2018)",
            "directori"=>'images/gow.jfif',
            "any"=>"2018",
            "peggi"=>"PG-18",
            "fabricant"=>"Santa Monica Studios",
            "tipus"=>7
        ]);
        DB::table("objectes")->insert([
            "nom"=>"PS5",
            "directori"=>'images/ps5.jfif',
            "any"=>"2020",
            "peggi"=>"None",
            "fabricant"=>"Sony Intercative Entertainment",
            "tipus"=>3
        ]);
        DB::table("objectes")->insert([
            "nom"=>"Xenoblade Chronicles 3",
            "directori"=>'images/xeno3.jfif',
            "any"=>"2022",
            "peggi"=>"PG-12",
            "fabricant"=>"Monolith Soft",
            "tipus"=>7
        ]);
        DB::table("objectes")->insert([
            "nom"=>"Ultimate Spider-Man DS",
            "directori"=>'images/Ultimate-Spiderman.jpg',
            "any"=>"2005",
            "peggi"=>"PG-7",
            "fabricant"=>"Vicarious Vision",
            "tipus"=>6
        ]);
    }
}
