<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/',[\App\Http\Controllers\HomeController::class, 'home']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'home'])->name('home');

//Rutas objecte
Route::get('/objecte',[\App\Http\Controllers\ObjectesController::class, 'getAllObjectes']);
Route::get('/objecte/add',[\App\Http\Controllers\ObjectesController::class, 'addObjecte']);
Route::get('/objecte/edit/{id}',[\App\Http\Controllers\ObjectesController::class, 'editObjecte']);
Route::post('/objecte/addStore',[\App\Http\Controllers\ObjectesController::class, 'addObjecteStore']);
Route::post('/objecte/editStore/{id}',[\App\Http\Controllers\ObjectesController::class, 'editObjecteStore']);

//Rutas subhasta
Route::get('/subhasta',[\App\Http\Controllers\SubhastesController::class, 'LasMevasSubhastes']);
Route::get('/subhasta/edit/{id}',[\App\Http\Controllers\SubhastesController::class, 'editSubhasta']);
Route::get('/subhasta/add',[\App\Http\Controllers\SubhastesController::class, 'addSubhasta']);
Route::post('/subhasta/addStore',[\App\Http\Controllers\SubhastesController::class, 'addSubhastaStore']);
Route::post('/subhasta/editStore/{id}',[\App\Http\Controllers\SubhastesController::class, 'editSubhastaStore']);

//Rutas Licitacio
Route::get('/licitacio',[\App\Http\Controllers\LicitacionsController::class, 'LesMevesLicitacions']);
Route::get('/licitacio/{id}',[\App\Http\Controllers\LicitacionsController::class, 'addLicitacio']);
Route::post('/licitacio/addStore/{id}',[\App\Http\Controllers\LicitacionsController::class, 'addLicitacioStore']);
Route::get('/licitacio/canviPreu/{id}',[\App\Http\Controllers\LicitacionsController::class, 'canviPreu']);


Route::get('/check',[\App\Http\Controllers\PlataformaController::class, "check"]);
