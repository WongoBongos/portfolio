<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipus_objecte extends Model
{
    use HasFactory;
    protected $table ="tipus_objectes";
    protected $primaryKey = "idtipus";
    protected $fillable = ["idtipus","nomtipus"];

    public function objecte(){
        return $this->hasMany(objectes::class);
    }

}
