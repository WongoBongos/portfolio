<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rols extends Model
{
    use HasFactory;
    protected $table ="rols";
    protected $primaryKey = "idrol";
    protected $fillable = ["idrol", "nom"];

    public function user(){
        return $this->hasMany(User::class);
    }
}
