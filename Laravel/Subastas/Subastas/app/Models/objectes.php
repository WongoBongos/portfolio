<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class objectes extends Model
{
    use HasFactory;
    protected $table ="objectes";
    protected $primaryKey = "idobjecte";
    protected $fillable =["idobjecte", "directori","any","peggi","fabricant","tipus"];

    public function subhasta(){
        return $this->hasMany(subhastes::class);
    }
    public function tipus(){
        return $this->belongsTo(Tipus_Objecte::class, "tipus", "idtipus");
    }
}
