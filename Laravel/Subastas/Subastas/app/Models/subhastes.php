<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subhastes extends Model
{
    use HasFactory;
    protected $table ="subhastes";
    protected $primaryKey = "idsubhasta";
    protected $fillable = ["idsubhasta","licitacio_maxima","licitacio_minima","data_finalitzacio", "activa", "objecte_subhastat", "preu_puja"];

    public function licitacio(){
        return $this->hasMany(licitacions::class,"idlicitacio");
    }
    public function objecte(){
        return $this->belongsTo(objectes::class, "objecte", "idobjecte");
    }
    public function plataforma(){
        return $this->belongsTo(plataforma::class, "idplataforma", "idplataforma");
    }
    public function user(){
        return $this->belongsTo(User::class, "jugador", "iduser");
    }
}
