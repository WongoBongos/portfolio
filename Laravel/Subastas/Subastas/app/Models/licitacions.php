<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class licitacions extends Model
{
    use HasFactory;
    protected $table ="licitacions";
    protected $primaryKey = "idlicitacio";
    protected $fillable = ["idlicitacio", "preuofert","subhasta"];

    public function subhasta(){
        return $this->belongsTo(Subhastes::class, 'subhasta', 'idsubhasta');
    }
    public function user(){
        return $this->belongsToMany(User::class);
    }

}
