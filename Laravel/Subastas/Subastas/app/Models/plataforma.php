<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class plataforma extends Model
{
    use HasFactory;
    protected $table ="plataformas";
    protected $primaryKey = "idplataforma";
    protected $fillable = ["idplataforma", "nom"];

    public function subhasta(){
        return $this->hasMany(subhastes::class);
    }
}
