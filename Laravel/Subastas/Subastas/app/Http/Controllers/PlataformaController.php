<?php

namespace App\Http\Controllers;

use App\Models\subhastes;
use Database\Seeders\user;
use Illuminate\Http\Request;

class PlataformaController extends Controller
{
   public function check(){
       $subhastas = subhastes::all();
       foreach ($subhastas as $subhasta){
           if (strtotime($subhasta->data_finalitzacio) < time()){
               $licitacioMesPagada = $subhasta->licitacio()->orderBy('preuofert', 'desc')->first();
                $user = \App\Models\User::find($subhasta->jugador);
                $plataformaimpuesto = ($licitacioMesPagada->preuofert * 10)/100;
                $ganancias = $licitacioMesPagada->preuofert - $plataformaimpuesto;
                $user->saldo = $user->saldo + $ganancias;
                $subhasta->objecte_subhastat = true;
                $subhasta->save();
                $user->save();
                return redirect('subhasta');
           }
           else{
               return redirect('subhasta');
           }
       }
   }
}
