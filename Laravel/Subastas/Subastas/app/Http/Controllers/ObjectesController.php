<?php

namespace App\Http\Controllers;

use App\Models\objectes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ObjectesController extends Controller
{
  public function getAllObjectes(){
      $objectes = objectes::all();
      if (\auth()->user()->rol == 4){
          return view("objectes.getAllObjectes",["objectes"=>$objectes]);
      }
      else if (\auth()->user()->rol == 2){
          return view("objectes.getAllObjectesVendedor",["objectes"=>$objectes]);
      }

  }
  public function addObjecte(){
    return view('objectes.addObjecte');
  }
  public function editObjecte(int $id){
    $objecte = objectes::find($id);
    return view("objectes.editObjecte",['objecte'=>$objecte]);
  }
  public function editObjecteStore(Request $request, int $id){
      $this->validate($request,[
          'nom' => 'required|string|max:255',
          'licitacio_maxima' => 'required|integer|max:9999',
          'peggi' => 'required|string|max:255',
          'fabricant' => 'required|string|max:255',
          'tipus' => 'required|exists:tipus_objectes,idtipus',
          'imatge' => 'required|image   |max:2048',
      ]);
        $imatge = $request->file("imatge");
        $nomimatge = $imatge->getClientOriginalName();
        $imatge->move(public_path('images'), $nomimatge);
        $objecte = objectes::find($id);
        $objecte->nom = $request->nom;
        $objecte->directori = "images/".$nomimatge;
        $objecte->any = $request->any;
        $objecte->peggi = $request->peggi;
        $objecte->fabricant = $request->fabricant;
        $objecte->tipus = $request->tipus;
        $objecte->save();
        return redirect('objecte');
  }


  public function addObjecteStore(Request $request){
      $this->validate($request,[
          'nom' => 'required|string|max:255',
          'any' => 'required|integer|max:9999',
          'peggi' => 'required|string|max:255',
          'fabricant' => 'required|string|max:255',
          'tipus' => 'required|exists:tipus_objectes,idtipus',
          'imatge' => 'required|image   |max:2048',
      ]);
      $imatge = $request->file("imatge");
      $nomimatge = $imatge->getClientOriginalName();
      $imatge->move(public_path('images'), $nomimatge);
      $objecte = new objectes();
      $objecte->nom = $request->nom;
      $objecte->directori = "images/".$nomimatge;
      $objecte->any = $request->any;
      $objecte->peggi = $request->peggi;
      $objecte->fabricant = $request->fabricant;
      $objecte->tipus = $request->tipus;
      $objecte->save();

      return redirect('objecte');
  }

}

