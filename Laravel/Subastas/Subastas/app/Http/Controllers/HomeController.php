<?php

namespace App\Http\Controllers;

use App\Models\licitacions;
use App\Models\subhastes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Sodium\add;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(){
        if (Auth::check()){
            if (\auth()->user()->rol == 4){
                $licitachions = array();
                $licitacions = licitacions::all();
                foreach ($licitacions as $licitacio){
                    if ($licitacio["iduser"] == \auth()->user()->iduser){
                       array_push($licitachions,$licitacio);
                    }
                }
                return view('licitacions.getAllLicitacions', ['licitachions' => $licitachions]);
            }
            else if (\auth()->user()->rol == 2){
                $subastes = array();
                $subhastes = subhastes::all();
                foreach ($subhastes as $subhasta){
                    if ($subhasta["jugador"] == \auth()->user()->iduser){
                        array_push($subastes,$subhasta);
                    }
                }
                return view('subhastas.getAllSubhastas', ['subastas' => $subastes]);
            }
        }else{
            return view('auth/login');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
