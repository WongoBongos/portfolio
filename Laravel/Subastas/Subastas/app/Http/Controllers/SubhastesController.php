<?php

namespace App\Http\Controllers;

use App\Models\licitacions;
use App\Models\plataforma;
use App\Models\subhastes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SubhastesController extends Controller
{
    public function LasMevasSubhastes(){
        if (auth()->user()->rol === 2){
            $subab = array();
            $licitacionesMax = array();
            $subhastes = subhastes::all();
            foreach ($subhastes as $subhasta){
                if ($subhasta["jugador"] == auth()->user()->iduser ){
                    array_push($subab, $subhasta);
                }

            }

            $subhastas = Subhastes::with(['licitacio' => function ($query) {
                $query->orderBy('preuofert', 'desc')->take(1);
            }])->get();
            foreach ($subhastas as $subhasta) {
                $licitacioMaxima = $subhasta->licitacio->first();
                if ($subhasta["jugador"] == auth()->user()->iduser ){
                    if ($licitacioMaxima) {
                        array_push($licitacionesMax, $licitacioMaxima);
                    }
                }
            }

            return view("subhastas.LesMevesSubhastes",["subhastes"=>$subab, "licitacionsMax"=>$licitacionesMax]);
        }else{
            return redirect('/');
        }

    }
    public function addSubhasta(){
        return view("subhastas.addSubhasta");
    }
    public function editSubhasta(int $id){
        $subhasta = subhastes::find($id);
        return view("subhastas.editSubhasta", ["subhasta"=>$subhasta]);
    }
    public function addSubhastaStore(Request $request){
        $this->validate($request,[
            'objecte' => 'required|exists:objectes,idobjecte',
            'licitacio_maxima' => 'required|numeric|max:9999999',
            'licitacio_minima' => 'required|numeric|max:9999999',
            'data_finalitzacio' => 'required|string|max:99999',
        ]);
        $subhasta = new subhastes();
        $subhasta->jugador = auth()->user()->iduser;
        $subhasta->objecte = $request->objecte;
        $subhasta->licitacio_maxima = $request->licitacio_maxima;
        $subhasta->licitacio_minima = $request->licitacio_minima;
        $subhasta->data_finalitzacio = $request->data_finalitzacio;
        $subhasta->licitacio_actual = $request->licitacio_maxima;
        $subhasta->activa = true;
        $subhasta->objecte_subhastat= false;
        $subhasta->preu_puja = false;
        $subhasta->idplataforma = plataforma::first()->idplataforma;
        $user = auth()->user();
        $user->saldo = $user->saldo - 50;
        $user->save();
        $subhasta->save();
        return redirect('subhasta');



    }
    public function editSubhastaStore(Request $request, int $id){
        $this->validate($request,[
            'objecte' => 'required|exists:objectes,idobjecte',
            'licitacio_maxima' => 'required|numeric|max:9999999',
            'licitacio_minima' => 'required|numeric|max:9999999',
            'data_finalitzacio' => 'required|string|max:99999',
        ]);
        $subhasta = subhastes::find($id);
        $subhasta->objecte = $request->objecte;
        $subhasta->licitacio_maxima = $request->licitacio_maxima;
        $subhasta->licitacio_minima = $request->licitacio_minima;
        $subhasta->data_finalitzacio = $request->data_finalitzacio;
        $subhasta->licitacio_actual = $request->licitacio_maxima;
        $subhasta->save();
       return redirect('subhasta');
    }
}
