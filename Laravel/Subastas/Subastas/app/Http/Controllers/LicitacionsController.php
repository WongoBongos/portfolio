<?php

namespace App\Http\Controllers;

use App\Models\licitacions;
use App\Models\subhastes;
use Illuminate\Http\Request;

class LicitacionsController extends Controller
{
    public function LesMevesLicitacions(){
        $licitacions = array();
        $licitaciones = licitacions::all();

        foreach ($licitaciones as $licitacio){
            if ($licitacio['iduser'] === auth()->user()->iduser){
                array_push($licitacions, $licitacio);
            }
        }
        return view("licitacions.LesMevesLicitacions",["licitacions"=>$licitacions]);

    }
    public function addLicitacio(int $idsubhasta){

        return view("licitacions.addLicitacio",["idsubhasta"=>$idsubhasta]);
    }
    public function addLicitacioStore(Request $request, int $idsubhasta){
            if ($request->preuofert <= auth()->user()->saldo && $request->preuofert >= subhastes::find($idsubhasta)->licitacio_actual){
                $licitacio = new licitacions();
                $licitacio->preuofert = $request->preuofert;
                $licitacio->iduser = auth()->user()->iduser;
                $licitacio->subhasta = $idsubhasta;
                $user = auth()->user();
                $user->saldo = $user->saldo - $licitacio->preuofert;
                $user->save();
                $licitacio->save();
                return redirect('licitacio');
            }
            else{
                return redirect("licitacio/$idsubhasta");
            }
    }
    public function canviPreu(int $id){
        $subhasta = subhastes::find($id);
        if ($subhasta->preu_puja == false){
            $porciento = ($subhasta->licitacio_actual * 5)/100;
            $subhasta->licitacio_actual = $subhasta->licitacio_actual - $porciento;
            if ($subhasta->licitacio_actual <= $subhasta->licitacio_minima){
                $subhasta->preu_puja = true;
            }
            $subhasta->save();
            return redirect('subhasta');
        }
        else{
            $porciento = ($subhasta->licitacio_actual * 5)/100;
            $subhasta->licitacio_actual = $subhasta->licitacio_actual + $porciento;
            if ($subhasta->licitacio_actual >= $subhasta->licitacio_minima){
                $subhasta->preu_puja = false;
            }
            $subhasta->save();
            return redirect('subhasta');
        }
    }
}
