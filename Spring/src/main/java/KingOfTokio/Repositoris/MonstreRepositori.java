package KingOfTokio.Repositoris;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import KingOfTokio.Model.Monstre;

public interface MonstreRepositori extends JpaRepository<Monstre, Integer>{
	
}
