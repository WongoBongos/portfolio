package KingOfTokio.Repositoris;

import org.springframework.data.jpa.repository.JpaRepository;

import KingOfTokio.Model.Partida;

public interface PartidaRepositori extends JpaRepository<Partida, Integer>{

}
