package KingOfTokio.Cotroller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import KingOfTokio.Services.MonstreServices;
import KingOfTokio.Services.PartidaServices;
import KingOfTokio.Model.Jugador;
import KingOfTokio.Model.Monstre;
import KingOfTokio.Model.Partida;
import KingOfTokio.Services.JugadorServices;

@Controller
public class MainController {

	@Autowired
	JugadorServices j;
	
	@Autowired
	MonstreServices m;
	
	@Autowired
	PartidaServices p;
	
	@GetMapping(path = "/") // Map ONLY GET Requests
	public @ResponseBody String welcome() {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		return "Estoy vivito y coleando!!!";
	}
	
	@GetMapping(path="/AllMonstresVius")
	public @ResponseBody List<Monstre> llistarMonstresVius() {

		List<Monstre> monstres = m.findAll();
		List<Monstre> monstresVius = new ArrayList<Monstre>();
		for (Monstre m : monstres) {
			if (m.getVides() > 0) {
				monstresVius.add(m);	
				System.out.println(m.getNomMonstre());
			}
		}
		return monstresVius;
	}
	@GetMapping(path = "/Jugador/add") 
	public @ResponseBody String addJugador(@RequestParam String name,  @RequestParam String apellidos) {
	

		Jugador ju = new Jugador();
		ju.setNom(name);
		ju.setCognom(apellidos); 


		j.insertar(ju);
		return "Saved";
	}
	
	@GetMapping(path = "/Partida/add") 
	public @ResponseBody String addPartida(@RequestParam int nJugadors) {


		Partida ju = new Partida();
		ju.setnJugadors(nJugadors);
		p.insertar(ju);
		return "Saved";
	}
	@GetMapping(path = "/Monstre/add") 
	public @ResponseBody String addMonstre(@RequestParam String name, @RequestParam int energy, @RequestParam boolean tokio, @RequestParam int vides ) {


		Monstre ju = new Monstre();
		ju.setNomMonstre(name);
		ju.setEnergia(energy);
		ju.setPuntsVictoria(0);
		ju.setEstaAToquio(tokio);
		ju.setVides(vides);

		m.insertar(ju);	
		return "Saved";
	}
	
	@GetMapping(path="/Jugador/Monstre")
	public @ResponseBody String addJugadorMonstre (@RequestParam int idJugador, @RequestParam int idMonstre) {
		
		Jugador jugador = j.findById(idJugador);
		Monstre monstre = m.findById(idMonstre);
		monstre.setId_jugador(jugador);
		jugador.getMonstres().add(monstre);
		m.editar(monstre);
		j.editar(jugador);
		return "Guardado!!!";
	}
	
	@GetMapping(path="/Partida/Monstre")
	public @ResponseBody String addPartidaMonstre(@RequestParam int idPartida, @RequestParam int idMonstre) {
		
		Partida partida = p.findById(idPartida);
		Monstre monstre = m.findById(idMonstre);
		monstre.setId_partida(partida);
		partida.getMonstres().add(monstre);
		m.editar(monstre);
		p.editar(partida);
		return "Guardado!!!";
	}
	
	
	@GetMapping(path="/AllMonstresViusContrincants/{id}")
	public @ResponseBody List<Monstre> LlistarMonstresViusContrincants(@PathVariable int id) {
		List<Monstre> monstres = m.findAll();
		List<Monstre> monstruos =  new ArrayList<Monstre>();
		System.out.println("nepe");
		for (Monstre mo : monstres) {
			if (mo.getId_monstre()!=id) {
				if (mo.getVides() > 0) {
					monstruos.add(mo);
				}
			}
		}
		return monstruos;
	}
	
	@GetMapping(path="/MonstreJugador/{player}")
	public @ResponseBody List<Monstre> LlistarMonstreJugador(@PathVariable int player) {
		Jugador jugador = j.findById(player);
		if (jugador == null)
			return null;
		Set<Monstre> monstresJugador = jugador.getMonstres();
		List<Monstre> ListMonstreDef = new ArrayList<Monstre>();
		for (Monstre mo : monstresJugador) {
			if (mo.getNomMonstre().contains("Alienoid") || mo.getNomMonstre().contains("MekaDracron")
					|| mo.getNomMonstre().contains("King") || mo.getNomMonstre().contains("Ciberkitty")
					|| mo.getNomMonstre().contains("Space Penguin") || mo.getNomMonstre().contains("Gigazaur")) {
				ListMonstreDef.add(mo);
			}
		}
		return ListMonstreDef;
	}

	
	

	@GetMapping(path="/HiHaMonstreATokyo")
	public @ResponseBody  boolean HiHaMonstreATokyo() {
		List<Monstre> monstres = m.findAll();
		for (Monstre mo : monstres) {
			if (mo.getVides() > 0 ) {
				if (mo.isEstaAToquio() == true) {
					return true;
				}
			}
		}
		return false;
	}
	
	@GetMapping(path="/GetMonstreTokyo")
	public @ResponseBody Monstre GetMonstreTokyo() {
		List<Monstre> monstres = m.findAll();
		boolean tokyo = HiHaMonstreATokyo();
		if (tokyo) {
			for (Monstre mo : monstres) {
				if (mo.isEstaAToquio() == true) {
					System.out.println("Monstre a Tokyo: " + mo.getNomMonstre());
					return mo;
				}
			}
		}
		return null;
	}


	
	public void SolveRoll(int id) {
		List<Integer> LlistaDaus = Roll();
		int contador1 = 0;
		int contador2 = 0;
		int contador3 = 0;
		for (int dado : LlistaDaus) {
			switch (dado) {
			case 1:
				contador1++;
				break;
			case 2:
				contador2++;
				break;
			case 3:
				contador3++;
				break;
			case 4:
				editarPuntsEnergia(id);

				break;
			case 5:
				eliminarVida(id);
				break;
			case 6:
				obtenirVida(id);
				break;

			default:
				break;
			}
		}

		if (contador1 == 3)
			donarPuntsVictoria(id, 1);

		if (contador2 == 3)
			donarPuntsVictoria(id, 2);

		if (contador3 == 3)
			donarPuntsVictoria(id, 3);
	}
	
	private void eliminarVida(int id) {
		List<Monstre> monstres = m.findAll();
		for (Monstre monstre : monstres) {
			if ((monstre.getNomMonstre().contains("King") && monstre.getId_jugador().getId_jugador()==id)|| (monstre.getNomMonstre().contains("MekaDracron") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Ciberkitty") && monstre.getId_jugador().getId_jugador()==id) || (monstre.getNomMonstre().contains("Gigazaur") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Space Penguin") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Alienoid") && monstre.getId_jugador().getId_jugador()==id)) {
				if (monstre.isEstaAToquio()) {

				}
			}
		}

	}

	private void obtenirVida(int id) {
		List<Monstre> monstres = m.findAll();
		for (Monstre monstre : monstres) {
			if ((monstre.getNomMonstre().contains("King") && monstre.getId_jugador().getId_jugador()==id)|| (monstre.getNomMonstre().contains("MekaDracron") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Ciberkitty") && monstre.getId_jugador().getId_jugador()==id) || (monstre.getNomMonstre().contains("Gigazaur") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Space Penguin") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Alienoid") && monstre.getId_jugador().getId_jugador()==id)) {
				monstre.setVides(monstre.getVides() + 1);
				m.editar(monstre);
			}
		}

	}
	

	private void editarPuntsEnergia(int id) {
		List<Monstre> monstres = m.findAll();
		for (Monstre monstre : monstres) {
			if ((monstre.getNomMonstre().contains("King") && monstre.getId_jugador().getId_jugador()==id)|| (monstre.getNomMonstre().contains("MekaDracron") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Ciberkitty") && monstre.getId_jugador().getId_jugador()==id) || (monstre.getNomMonstre().contains("Gigazaur") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Space Penguin") && monstre.getId_jugador().getId_jugador()==id)
					|| (monstre.getNomMonstre().contains("Alienoid") && monstre.getId_jugador().getId_jugador()==id)) {
				monstre.setEnergia(monstre.getEnergia() + 1);
				m.editar(monstre);
			}
		}
	}


	

	@GetMapping(path="/tirada/{idPartida}")
	public @ResponseBody String tirada(@PathVariable int idPartida) {
		List<Jugador> orden = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						orden.add(jugador);
					}
				}
			}
		}
		SolveRoll(orden.get(0).getId_jugador());

		return "Ha salido bien";
			}
	

	public Jugador getCurrentPlayer(List<Jugador> orden) {
		return orden.get(0);
	}



	@GetMapping(path="/passarTorn/{idPartida}")
	public @ResponseBody List<Jugador> passarTorn(@PathVariable int idPartida) {
		List<Jugador> orden = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						orden.add(jugador);
					}
				}
			}
		}
		orden.add(orden.size(), orden.get(0));
		orden.remove(0);
		return orden;
	}
	
	public int CountMostresVius(List<Jugador> orden)
	{int contador=0;
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if(monstre.getVides()>0)
					contador++;
			}
		}
		return contador;
	}
	
	
	@GetMapping(path="/Reassign/{idPartida}")
	public @ResponseBody void Reassign(@PathVariable int idPartida)
	{
		List<Jugador> orden = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						orden.add(jugador);
					}
				}
			}
		}
		this.ComprobacionesVictoria(orden);
		this.CountMostresVius(orden);
		if(this.HiHaMonstreATokyo())
			this.SetMonstreTokioAleatori(orden);
	}
	

	public void SetMonstreTokioAleatori(List<Jugador> orden) {
		List<Monstre> totsElsMonstres=new ArrayList<Monstre>();
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				totsElsMonstres.add(monstre);
			}
		}
		Collections.shuffle(totsElsMonstres);
		Monstre monstreTokio=totsElsMonstres.get(0);
		monstreTokio.setEstaAToquio(true);
	}
	
	
	public void SetMonstreTokioAleatori() {
		
		List<Monstre> monstres = m.findAll();
		if (!HiHaMonstreATokyo()) {
			Collections.shuffle(monstres);
			monstres.get(0).setEstaAToquio(true);
			m.editar(monstres.get(0));
		}
	}
	@GetMapping(path="/MonstresDePoderLliure")
	public @ResponseBody List<Monstre> LlistarMonstresDePoderLliure() {

		List<Monstre> monstres = m.findAll();
		List<Monstre> poder = new ArrayList<Monstre>();
		List<Monstre> vivos = llistarMonstresVius();
		List<Monstre> libres = new ArrayList<Monstre>();

		for (Monstre mo : monstres) {
			if (!mo.getNomMonstre().contains("Alienoid") || !mo.getNomMonstre().contains("MekaDracron")
					|| !mo.getNomMonstre().contains("King") || !mo.getNomMonstre().contains("Ciberkitty")
					|| !mo.getNomMonstre().contains("Space Penguin") || !mo.getNomMonstre().contains("Gigazaur")) {
				poder.add(mo);
			}
		}

		for (Monstre mon : poder) {
			for (Monstre mos : vivos) {
				if (mos.getMonstre_de_poder().getId_monstre() == mon.getId_monstre()) {
					System.out.println("hola");
				} else {
					libres.add(mon);
					System.out.println(mon.getNomMonstre());
				}
			}
		}
		return libres;
	}
	@GetMapping(path="/Roll")
	public @ResponseBody List<Integer> Roll() {
		List<Integer> LlistaDaus = new ArrayList<Integer>();
		for (int i = 0; i < 6; i++) {
			int rd = (int) (Math.random() * 6 + 1);
			System.out.println("Ha sortit un " + rd + "!");
			LlistaDaus.add(rd);
		}
		return LlistaDaus;
	}
	private void donarPuntsVictoria(int idJugador, int i) {
		
		Jugador Jug = null;
		for (Jugador Player : j.findAll()) {
			if (Player.getId_jugador() == idJugador) {
				Jug = Player;
			}
		}
		if (Jug == null)
			return;
		Set<Monstre> monstres = Jug.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
					|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
					|| monstre.getNomMonstre().contains("Space Penguin")
					|| monstre.getNomMonstre().contains("Alienoid")) {
				monstre.setPuntsVictoria(monstre.getPuntsVictoria() + i);
				m.editar(monstre);
			}
		}

	}
	public  boolean ComprobacionesVictoria(List<Jugador> orden) {
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if (monstre.getPuntsVictoria() > 20) {
					return true;
				}
			}
		}
		return false;
	}
	@GetMapping(path="/determinarTornInicial/{idPartida}")
	public @ResponseBody List<Jugador> determinarTornInicial(@PathVariable int idPartida) {
		List<Jugador> JugadoresOrdenados = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						JugadoresOrdenados.add(jugador);
					}
				}
			}
		}
		Collections.shuffle(JugadoresOrdenados);
		return JugadoresOrdenados;
	}
	@GetMapping(path="/MonstreMaxPuntVictoria/{idPartida}")
	public @ResponseBody Monstre MonstreMaxPuntVictoria(@PathVariable int idPartida) {
		List<Jugador> orden = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						orden.add(jugador);
					}
				}
			}
		}
		Monstre monstree = new Monstre();
		monstree.setPuntsVictoria(0);
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if(monstre.getPuntsVictoria() > monstree.getPuntsVictoria())
					monstree = monstre;
			}
		}
		return monstree;
	}
	@GetMapping(path="/ActualitzarMonstresVius/{idPartida}")
	public @ResponseBody String ActualitzarMonstresVius(@PathVariable int idPartida)
	{
		List<Jugador> orden = new ArrayList<Jugador>();
		for (Jugador jugador : j.findAll()) {
			for (Monstre monstre : jugador.getMonstres()) {
				if (monstre.getId_partida().getId_partida() == idPartida) {
					if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
							|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
							|| monstre.getNomMonstre().contains("Space Penguin")
							|| monstre.getNomMonstre().contains("Alienoid")) {
						orden.add(jugador);
					}
				}
			}
		}
		String frase = "No se ha eliminado ningun Monstre";
		for (Jugador j : orden) {
		Set<Monstre> monstres = j.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getVides() <= 0);
				monstres.remove(monstre);
				frase = "Elimats monstres Correctament";
			}
		}
		return frase;
	}
}
