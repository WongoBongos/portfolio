package KingOfTokio.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import KingOfTokio.Model.Monstre;
import KingOfTokio.Repositoris.MonstreRepositori;

@Service
public class MonstreServices {
	@Autowired
	MonstreRepositori repository;
	
	public Monstre findById(Integer id) {
		return repository.findById(id).orElse(null);
	}

	public List<Monstre> findAll() {
		return repository.findAll();
	}

	public void delete(Integer id) {

		repository.deleteById(id);
	}

	public Monstre insertar(Monstre j) {
		return repository.save(j);
	}

	public Monstre editar(Monstre j) {
		return repository.save(j);
	}
	
}