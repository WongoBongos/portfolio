package KingOfTokio.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Partida")
public class Partida {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_partida", updatable = false)
	private int id_partida;
	
	
	@Column(name = "torn", updatable = true,nullable=false)
	private int torn;
	
	@Column(name = "nJugadors",  updatable = true,nullable=false)
	private int nJugadors;
	
	@OneToMany(mappedBy = "id_partida")
	private Set<Monstre> monstres = new HashSet<Monstre>();

	public Partida() {
		super();
	}

	@Override
	public String toString() {
		return "Partida [id_partida=" + id_partida + ", torn=" + torn + ", nJugadors=" + nJugadors + ", monstres="
				+ monstres + "]";
	}

	public Partida(int torn, int nJugadors, Set<Monstre> monstres) {
		super();
		this.torn = torn;
		this.nJugadors = nJugadors;
		this.monstres = monstres;
	}

	public int getId_partida() {
		return id_partida;
	}

	public void setId_partida(int id_partida) {
		this.id_partida = id_partida;
	}

	public int getTorn() {
		return torn;
	}

	public void setTorn(int torn) {
		this.torn = torn;
	}

	public int getnJugadors() {
		return nJugadors;
	}

	public void setnJugadors(int nJugadors) {
		this.nJugadors = nJugadors;
	}

	public Set<Monstre> getMonstres() {
		return monstres;
	}

	public void setMonstres(Set<Monstre> monstres) {
		this.monstres = monstres;
	}
}
