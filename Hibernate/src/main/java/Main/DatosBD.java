package Main;

import java.util.HashSet;
import java.util.Set;

import MapeadoClases.Jugador;
import MapeadoClases.Monstre;
import MapeadoClases.Partida;
import bdDAO.JugadorDAO;
import bdDAO.MonstreDAO;
import bdDAO.PartidaDAO;

public class DatosBD {

	private static JugadorDAO jd = new JugadorDAO();
	private static MonstreDAO md = new MonstreDAO();
	private static PartidaDAO pd = new PartidaDAO();
	
	public static void CardsMonstre() {

		Monstre Aliento_Flamígero = new Monstre();
		Monstre Mimetismo = new Monstre();
		Monstre Rayo_Reductor = new Monstre();
		Monstre Escupidor_Veneno = new Monstre();

		md.Insert(Aliento_Flamígero);
		md.Insert(Mimetismo);
		md.Insert(Rayo_Reductor);
		md.Insert(Escupidor_Veneno);

		Aliento_Flamígero.setNomMonstre("Aliento_Flamígero");
		Mimetismo.setNomMonstre("Mimetismo");
		Rayo_Reductor.setNomMonstre("Rayo_Reductor");
		Escupidor_Veneno.setNomMonstre("Escupidor_Veneno");

		md.Update(Aliento_Flamígero);
		md.Update(Mimetismo);
		md.Update(Rayo_Reductor);
		md.Update(Escupidor_Veneno);
	}
	
	public static Partida newPartida(int numJugadors) {
		
		Partida p  = new Partida();

		pd.Insert(p);
		
		p.setnJugadors(numJugadors);
		p.setTorn(1);
				
		Monstre King = new Monstre();
		Monstre MekaDracron = new Monstre();
		Monstre Ciberkitty = new Monstre();
		Monstre Gigazaur = new Monstre();
		Monstre Space_Penguin = new Monstre();
		Monstre Alienoid = new Monstre();
		
		md.Insert(King);
		md.Insert(MekaDracron);
		md.Insert(Ciberkitty);
		md.Insert(Gigazaur);
		md.Insert(Space_Penguin);
		md.Insert(Alienoid);
		
		setMonstre(King, "King", 0, 5, p);
		setMonstre(MekaDracron, "MekaDracron", 0, 5, p);
		setMonstre(Ciberkitty, "Ciberkitty", 0, 5, p);
		setMonstre(Gigazaur, "Gigazaur", 0, 5, p);
		setMonstre(Space_Penguin, "Space_Penguin", 0, 5, p);
		setMonstre(Alienoid, "Alienoid", 0, 5, p);
		
		md.Update(King);
		md.Update(MekaDracron);
		md.Update(Ciberkitty);
		md.Update(Gigazaur);
		md.Update(Space_Penguin);
		md.Update(Alienoid);
		
		p.getMonstres().add(King);
		p.getMonstres().add(MekaDracron);
		p.getMonstres().add(Ciberkitty);
		p.getMonstres().add(Gigazaur);
		p.getMonstres().add(Space_Penguin);
		p.getMonstres().add(Alienoid);
		
		pd.Update(p);
		
		return p;
	}
	
	private static void setMonstre(Monstre m, String nom, int Energia, int vides, Partida p) {
		m.setNomMonstre(nom);
		m.setEnergia(Energia);
		m.setPuntsVictoria(0);
		m.setVides(vides);
		m.setId_partida(p);
	}
	
	public static Jugador setJugadors(Monstre m, String nom, String cogNom) {
		
		Jugador j = new Jugador();
		
		jd.Insert(j);
		
		j.setCognom(cogNom);
		j.setNom(nom);
		j.getMonstres().add(m);
		
		jd.Update(j);
		
		m.setId_jugador(j);
		
		md.Update(m);
		
		return j;
	}
}
