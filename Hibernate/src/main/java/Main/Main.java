package Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import Funciones.Manager;
import MapeadoClases.Jugador;
import MapeadoClases.Monstre;
import MapeadoClases.Partida;
import bdDAO.JugadorDAO;
import bdDAO.MonstreDAO;
import bdDAO.PartidaDAO;

public class Main {

	static boolean jugando = true;
	private static Scanner sc = new Scanner(System.in);
	private static Random rd = new Random();


	public static void main(String[] args) {
		Menu();
	}

	public static void jugarPartida(Manager ma, List<Jugador> jugadores) {
		JugadorDAO jd = new JugadorDAO();
		MonstreDAO md = new MonstreDAO();
		PartidaDAO pd = new PartidaDAO();
		
		boolean fi = false;
		ma.determinarTornInicial(jugadores);
		Monstre m = ma.getMonstreJugador(jugadores.get(0));
		m.setEstaAToquio(true);
		md.Update(m);
		while (!fi) {
			System.out.println("vides del monstre a tokio "+ma.GetMonstreTokyo().getVides());
			int vida = ma.GetMonstreTokyo().getVides();
			ma.tirada(jugadores);
			if (vida > ma.GetMonstreTokyo().getVides()) {
				int num = rd.nextInt(0,2);
				if (num == 0) {
					Monstre mo = ma.GetMonstreTokyo();
					mo.setEstaAToquio(false);
					md.Update(mo);
					Monstre mo2 = ma.getMonstreJugador(jugadores.get(0));
					mo2.setEstaAToquio(true);
					md.Update(mo2);
				}
			}
			int num = rd.nextInt(0,2);
			if (ma.getMonstreJugador(jugadores.get(0)).getMonstre_de_poder() != null) {
				if (num == 0)
					ma.UtilitzarCartaPoder(ma.getMonstreJugador(jugadores.get(0)));
			}
			else
				if (num == 0)
					ma.ComprarCarta(ma.getMonstreJugador(jugadores.get(0)));
			fi = ma.ComprobacionesVictoria(jugadores);
			jugadores = ma.passarTorn(jugadores);
		}
		System.out.println("PARTIDA FINALITZADA");
	
	}

	public static void Menu() {
		System.out.println("Pulsa 1 para iniciar partida");
		System.out.println("Pulsa 2 para dejar de jugar");
		JugadorDAO jd = new JugadorDAO();
		MonstreDAO md = new MonstreDAO();
		PartidaDAO pd = new PartidaDAO();
		
		int menuOption = sc.nextInt();
		
		switch (menuOption) {
		case 1:
			DatosBD.CardsMonstre();
			Random rd = new Random();
			int players = rd.nextInt(2,5);
			Partida p = DatosBD.newPartida(players);
			Manager ma = new Manager(jd, pd, md, p);
			List<Monstre> ListM = ma.llistarMonstresVius();
			Collections.shuffle(ListM);
			List<Jugador> List = new ArrayList<Jugador>();
			for (int i = 0; i < players; i++) {
				Jugador j = DatosBD.setJugadors(ListM.get(i), "Alfredo" + i, "Hernandez" + i);
				List.add(j);
			}
			jugarPartida(ma, List);
			break;
		case 2:
			jugando = !jugando;
			break;

		}
	}
}
