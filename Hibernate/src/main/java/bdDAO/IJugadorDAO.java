package bdDAO;

import java.io.Serializable;

import MapeadoClases.Jugador;

public interface IJugadorDAO extends IGenericDAO<Jugador, Integer>, Serializable {

}
