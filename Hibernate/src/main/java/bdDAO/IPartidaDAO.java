package bdDAO;

import java.io.Serializable;

import MapeadoClases.Partida;

public interface IPartidaDAO extends IGenericDAO<Partida, Integer>, Serializable{

}
