package Funciones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import MapeadoClases.Jugador;
import MapeadoClases.Monstre;
import MapeadoClases.Partida;
import bdDAO.JugadorDAO;
import bdDAO.MonstreDAO;
import bdDAO.PartidaDAO;

public class Manager {
	private static Random rd = new Random();
	private static boolean m1=true;
	private static boolean m2=true;
	private static boolean m3=true;
	private static boolean m4=true;
	JugadorDAO jd;
	PartidaDAO pd;
	MonstreDAO md;
	Partida p;

	public Manager(JugadorDAO jd, PartidaDAO pd, MonstreDAO md, Partida pa) {
		this();
		this.jd = jd;
		this.pd = pd;
		this.md = md;
		this.p = pa;
	}

	public Manager() {
		super();
	}

	public List<Monstre> llistarMonstresVius() {

		List<Monstre> monstres = md.listar();
		List<Monstre> MonstresVius = new ArrayList<Monstre>();
		for (Monstre m : monstres) {
			if (m.getVides() > 0 && p.getId_partida() == m.getId_partida().getId_partida()) {
				System.out.println(m.getNomMonstre());
				MonstresVius.add(m);
			}
		}
		return MonstresVius;
	}

	public void LlistarMonstresViusContrincants(Monstre m) {

		List<Monstre> monstres = md.listar();

		for (Monstre mo : monstres) {
			if (!mo.getNomMonstre().equals(m.getNomMonstre())
					&& p.getId_partida() == mo.getId_partida().getId_partida()) {
				if (mo.getVides() > 0) {
					System.out.println(mo.getNomMonstre());
				}
			}
		}

	}

	public void LlistarMonstreJugador(Jugador jugador) {

		Set<Monstre> monstresJugador = jugador.getMonstres();
		for (Monstre mo : monstresJugador) {
			if (mo.getNomMonstre().contains("Alienoid") || mo.getNomMonstre().contains("MekaDracron")
					|| mo.getNomMonstre().contains("King") || mo.getNomMonstre().contains("Ciberkitty")
					|| mo.getNomMonstre().contains("Space_Penguin") || mo.getNomMonstre().contains("Gigazaur")
							&& p.getId_partida() == mo.getId_partida().getId_partida()) {
				System.out.println(mo.getNomMonstre());
				System.out.println(mo.getEnergia());
				System.out.println(mo.getVides());
				System.out.println(mo.getPuntsVictoria());

			}
		}

	}

	public void LlistarMonstresDePoderLliure() {

		List<Monstre> monstres = md.listar();
		List<Monstre> poder = new ArrayList<Monstre>();
		List<Monstre> vivos = new ArrayList<Monstre>();
		List<Monstre> libres = new ArrayList<Monstre>();

		for (Monstre m : monstres) {
			if (m.getVides() > 0 && p.getId_partida() == m.getId_partida().getId_partida()) {
				vivos.add(m);
			}
		}

		for (Monstre mo : monstres) {
			if (!mo.getNomMonstre().contains("Alienoid") || !mo.getNomMonstre().contains("MekaDracron")
					|| !mo.getNomMonstre().contains("King") || !mo.getNomMonstre().contains("Ciberkitty")
					|| !mo.getNomMonstre().contains("Space_Penguin") || !mo.getNomMonstre().contains("Gigazaur")
							&& p.getId_partida() == mo.getId_partida().getId_partida()) {
				poder.add(mo);
			}
		}

		for (Monstre mon : poder) {
			for (Monstre mos : vivos) {

				if (mos.getMonstre_de_poder().getId_monstre() == mon.getId_monstre()
						&& p.getId_partida() == mon.getId_partida().getId_partida()
						&& p.getId_partida() == mos.getId_partida().getId_partida()) {

					System.out.println("hola");
				} else {
					libres.add(mon);
					System.out.println(mon.getNomMonstre());
				}
			}

		}

	}

	public boolean HiHaMonstreATokyo() {
		List<Monstre> monstres = md.listar();
		for (Monstre mo : monstres) {
			if (mo.getVides() > 0 && p.getId_partida() == mo.getId_partida().getId_partida()) {
				if (mo.isEstaAToquio() == true) {
					return true;
				}
			}
		}
		return false;
	}

	public Monstre GetMonstreTokyo() {
		List<Monstre> monstres = md.listar();
		boolean tokyo = HiHaMonstreATokyo();
		if (tokyo) {
			for (Monstre mo : monstres) {
				if (mo.isEstaAToquio() == true && p.getId_partida() == mo.getId_partida().getId_partida()) {
					System.out.println("Monstre a Tokyo: " + mo.getNomMonstre());
					
					return mo;
				}
			}
		}	
		return null;
	}

	public List<Integer> Roll() {
		List<Integer> LlistaDaus = new ArrayList<Integer>();
		for (int i = 0; i < 6; i++) {
			int rd = (int) (Math.random() * 6 + 1);
			System.out.println("Ha sortit un " + rd + "!");
			LlistaDaus.add(rd);
		}
		return LlistaDaus;
	}

	public void SolveRoll(Jugador j) {
		List<Monstre> ms = md.listar();
		System.out.println("Abans de resoldre daus");
		for(Monstre m: ms) {
			System.out.println(m.toString());	
		}
		List<Integer> LlistaDaus = Roll();
		int contador1 = 0;
		int contador2 = 0;
		int contador3 = 0;
		for (int dado : LlistaDaus) {
			switch (dado) {
			case 1:
				contador1++;
				break;
			case 2:
				contador2++;
				break;
			case 3:
				contador3++;
				break;
			case 4:
				editarPuntsEnergia(j);

				break;
			case 5:
				eliminarVida(j);
				break;
			case 6:
				obtenirVida(j);
				break;

			default:
				break;
			}
		}

		if (contador1 == 3)
			donarPuntsVictoria(j, 1);

		if (contador2 == 3)
			donarPuntsVictoria(j, 2);

		if (contador3 == 3)
			donarPuntsVictoria(j, 3);
		
		ms = md.listar();
		
		System.out.println("Després de resoldre daus");
		for(Monstre m: ms) {
		System.out.println(m.toString());	
		}
	}

	private void eliminarVida(Jugador j) {
		Set<Monstre> monstres = j.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
					|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
					|| monstre.getNomMonstre().contains("Space_Penguin")
					|| monstre.getNomMonstre().contains("Alienoid")) {
				if (monstre.isEstaAToquio()) {
					List<Monstre> monstruos = md.listar();

					for (Monstre m : monstruos) {
						if (!m.isEstaAToquio() && m.getVides() > 0
								&& p.getId_partida() == m.getId_partida().getId_partida()) {
							m.setVides(m.getVides() - 1);
							md.Update(m);
						}
					}
				} else {
					List<Monstre> monstruos = md.listar();

					for (Monstre m : monstruos) {
						if (m.isEstaAToquio() && m.getVides() > 0
								&& p.getId_partida() == m.getId_partida().getId_partida()) {
							m.setVides(m.getVides() - 1);
							md.Update(m);
						}
					}
				}
			}
		}

	}

	private void donarPuntsVictoria(Jugador j, int i) {
		Set<Monstre> monstres = j.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
					|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
					|| monstre.getNomMonstre().contains("Space_Penguin")
					|| monstre.getNomMonstre().contains("Alienoid")) {
				monstre.setPuntsVictoria(monstre.getPuntsVictoria() + i);
				md.Update(monstre);
			}
		}

	}

	private void obtenirVida(Jugador j) {
		Set<Monstre> monstres = j.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
					|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
					|| monstre.getNomMonstre().contains("Space_Penguin")
					|| monstre.getNomMonstre().contains("Alienoid")) {
				monstre.setVides(monstre.getVides() + 1);
				md.Update(monstre);
			}
		}

	}

	private void editarPuntsEnergia(Jugador j) {
		Set<Monstre> monstres = j.getMonstres();
		for (Monstre monstre : monstres) {
			if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
					|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
					|| monstre.getNomMonstre().contains("Space_Penguin")
					|| monstre.getNomMonstre().contains("Alienoid")) {
				monstre.setEnergia(monstre.getEnergia() + 1);
				md.Update(monstre);
			}
		}
	}

	public boolean ComprobacionesVictoria(List<Jugador> orden) {
		int cont=0;
		for (Jugador jugador : orden) {
			if (jugador.getMonstres().size() == 0) {
				orden.remove(cont);
			}
			cont++;
		}
		if(orden.size()==1)
		{
			Set<Monstre> listaGanador=orden.get(0).getMonstres();
			for(Monstre m:listaGanador)
			{
				for(Jugador j:orden) {
					if(j.getId_jugador()==m.getId_jugador().getId_jugador())
					{
						System.out.println("Guanyador: "+j.getNom()+" "+j.getCognom());
					}	
				}
				System.out.println("Monstre: "+m.getNomMonstre());
			}
			
			return true;
		}
		// Comprobacion por puntos de victoria
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if (monstre.getPuntsVictoria() > 20) {
					System.out.println("Guanyador: "+j.getNom()+" "+j.getCognom());
					System.out.println("Monstre: "+monstre.getNomMonstre());
					
					return true;
				}
			}
		}
		return false;
	}

	public void tirada(List<Jugador> orden) {
		SolveRoll(orden.get(0));
	}

	public Jugador getCurrentPlayer(List<Jugador> orden) {
		return orden.get(0);
	}

	public List<Jugador> determinarTornInicial(List<Jugador> jugadoresDesordenados) {
		List<Jugador> JugadoresOrdenados = jugadoresDesordenados;
		Collections.shuffle(JugadoresOrdenados);

		return JugadoresOrdenados;
	}

	public List<Jugador> passarTorn(List<Jugador> orden) {
		orden.add(orden.size(), orden.get(0));
		orden.remove(0);
		return orden;
	}

	public int CountMostresVius(List<Jugador> orden) {
		int contador = 0;
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if (monstre.getVides() > 0)
					contador++;
			}
		}
		return contador;
	}

	public Monstre MonstreMaxPuntVictoria(List<Jugador> orden) {
		Monstre monstree = new Monstre();
		monstree.setPuntsVictoria(0);
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if (monstre.getPuntsVictoria() > monstree.getPuntsVictoria())
					monstree = monstre;
			}
		}
		return monstree;
	}

	public void Reassign(List<Jugador> orden) {
		this.ComprobacionesVictoria(orden);
		this.CountMostresVius(orden);
		if (this.HiHaMonstreATokyo())
			this.SetMonstreTokioAleatori(orden);
	}

	public void SetMonstreTokioAleatori(List<Jugador> orden) {
		List<Monstre> totsElsMonstres = new ArrayList<Monstre>();
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				totsElsMonstres.add(monstre);
			}
		}
		Collections.shuffle(totsElsMonstres);
		Monstre monstreTokio = totsElsMonstres.get(0);
		monstreTokio.setEstaAToquio(true);
	}

	public void ActualitzarMonstresVius(List<Jugador> orden) {
		for (Jugador j : orden) {
			Set<Monstre> monstres = j.getMonstres();
			for (Monstre monstre : monstres) {
				if (monstre.getVides() == 0)
					;
				monstres.remove(monstre);
			}
		}

	}

	public void SetMonstreTokioAleatori() {
		List<Monstre> monstrew = new ArrayList<Monstre>();
		List<Monstre> monstres = md.listar();
		for(Monstre monstre :monstres) {
		if (monstre.getNomMonstre().contains("King") || monstre.getNomMonstre().contains("MekaDracron")
				|| monstre.getNomMonstre().contains("Ciberkitty") || monstre.getNomMonstre().contains("Gigazaur")
				|| monstre.getNomMonstre().contains("Space_Penguin")
				|| monstre.getNomMonstre().contains("Alienoid")) {
			monstrew.add(monstre);
		}
		
		}
		
		if (!HiHaMonstreATokyo()) {
			Collections.shuffle(monstrew);
			monstrew.get(0).setEstaAToquio(true);
			md.Update(monstrew.get(0));
		}
	}

	public Monstre UnMonstreViu() {
		List<Monstre> monstres = md.listar();
		List<Monstre> monstruo = md.listar();

		for (Monstre m : monstres) {
			if (m.getVides() > 0 && p.getId_partida() == m.getId_partida().getId_partida()) {
				monstruo.add(m);
			}
		}
		if (monstruo.size() == 1)
			return monstruo.get(0);
		return null;
	}

	public void SolvePowerCarts(Monstre m) {
		Scanner sc = new Scanner(System.in);
		if (m.getMonstre_de_poder()!= null) {
			System.out.println("Que vols fer amb la carta de poder?");
			System.out.println("1.-Utilitzar-la");
			System.out.println("2.-Esperar");

			int num = sc.nextInt();
			switch (num) {
			case 1:
				UtilitzarCartaPoder(m);
				break;
			case 2:
				System.out.println("Esperarem");
				break;
			default:
				break;
			}
		} else {
			System.out.println("No tens cap monstre de poder, vols comprar-ne un?");
			System.out.println("1.-Si");
			System.out.println("2.-No");
			int num = sc.nextInt();
			switch (num) {
			case 1:
				ComprarCarta(m);
				break;
			case 2:
				System.out.println("Esperarem");
				break;
			default:
				break;
			}
		}
		sc.close();

	}

	public void ComprarCarta(Monstre m) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quina  carta vols comprar?");
		System.out.println("1.-Aliento Flamigero -3 Unitats");
		System.out.println("2.-Mimetismo -8 Unitats");
		System.out.println("3.-Monstruo con Rayo Reductor -6 Unitats");
		System.out.println("4.-Monstruo Escupidor de Veneno - 4 Unitats");
		int num = rd.nextInt(1,5);
		switch (num) {
		case 1:
			if (m.getEnergia() > 3)
			{
				if(m.getMonstre_de_poder() == null) {
					m.setEnergia(m.getEnergia()-3);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Aliento_Flamígero")&& m1 ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m1=false;
						}
					}
				}
				else {
					m.setEnergia(m.getEnergia()-3);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Aliento_Flamígero") && m.getMonstre_de_poder().getId_monstre() != mon.getId_monstre()&& m1) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m1=false;
						}
					}
				}
			
			}
		
			md.Update(m);
			break;
		case 2:
			if (m.getEnergia() > 8)	
			{
				
				if(m.getMonstre_de_poder() == null) {
					m.setEnergia(m.getEnergia()-8);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Mimetismo")&& m2) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m2=false;
						}
					}
				}
				else {
					m.setEnergia(m.getEnergia()-8);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Mimetismo") && m.getMonstre_de_poder().getId_monstre() != mon.getId_monstre()&& m2 ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m2=false;
						}
					}
				}
			
			}
			
			md.Update(m);
			break;
		case 3:
			if (m.getEnergia() > 6)
			{
				
				if(m.getMonstre_de_poder() == null) {
					m.setEnergia(m.getEnergia()-6);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Rayo_Reductor") && m3 ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m3=false;
						}
					}
				}
				else {
					m.setEnergia(m.getEnergia()-6);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Rayo_Reductor") && m.getMonstre_de_poder().getId_monstre() != mon.getId_monstre() && m3 ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m3=false;
						}
					}
				}
				
			}
		
			md.Update(m);
			break;
		case 4:
			if (m.getEnergia() > 4)
			{
				if(m.getMonstre_de_poder() == null) {
					m.setEnergia(m.getEnergia()-4);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Escupidor_Veneno") && m4  ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m4=false;
						}
					}
				}
				else {
					m.setEnergia(m.getEnergia()-4);
					List<Monstre> monstres = md.listar();			
					for (Monstre mon : monstres) {
						if (mon.getNomMonstre().contains("Escupidor_Veneno") && m.getMonstre_de_poder().getId_monstre() != mon.getId_monstre() && m4 ) {
							m.setMonstre_de_poder(mon);
							System.out.println("Comprado: "+m.getMonstre_de_poder().getNomMonstre());
							m4=false;
						}
					}
				}
			}
		
			md.Update(m);
			break;

		default:
			break;
		}
		
		sc.close();
	}

	public void UtilitzarCartaPoder(Monstre m) {
		String nom = m.getMonstre_de_poder().getNomMonstre();
		System.out.println("USANDO CARTA"+nom);
		if (nom.contains("Aliento_Flamígero") && m.getEnergia() > 3) {
			Set<Monstre> a = p.getMonstres();
			for (Monstre mon : a) {
				if (!mon.getNomMonstre().contains(m.getNomMonstre())) {
					System.out.println("Monstuo antes de pegarse");
					System.out.println(mon.toString());
					mon.setVides(mon.getVides() - 1);
					md.Update(mon);
					m.setMonstre_de_poder(null);
					md.Update(m);
					m1=true;
					System.out.println("Monstuo despues de pegarse");
					System.out.println(mon.toString());
				}
			}
		}
		if (nom.contains("Mimetismo") && m.getEnergia() > 8) {
			int numero = (int) (Math.random() * p.getnJugadors());
			int contador = 0;
			int videsM = m.getVides();
			int puntsM = m.getPuntsVictoria();
			Set<Monstre> a = p.getMonstres();
			for (Monstre mon : a) {
				if (!mon.getNomMonstre().contains(m.getNomMonstre())) {
					if (contador == numero) {
						System.out.println("Monstuo antes de pegarse");
						System.out.println(mon.toString());
						m.setVides(mon.getVides());
						m.setPuntsVictoria(mon.getPuntsVictoria());
						mon.setPuntsVictoria(puntsM);
						mon.setVides(videsM);
						m.setMonstre_de_poder(null);
						m2=true;
						md.Update(mon);
						md.Update(m);
						System.out.println("Monstuo despues de pegarse");
						System.out.println(mon.toString());
						
					}
				}
				contador++;
			}
		}
		if (nom.contains("Rayo_Reductor") && m.getEnergia() > 6) {
			int numero = (int) (Math.random() * p.getnJugadors());
			int contador = 0;
			Set<Monstre> a = p.getMonstres();
			for (Monstre mon : a) {
				if (!mon.getNomMonstre().contains(m.getNomMonstre())) {
					if (contador == numero) {
						System.out.println("Monstuo antes de pegarse");
						System.out.println(mon.toString());
						mon.setVides(mon.getVides() - 1);
						md.Update(mon);
						m.setMonstre_de_poder(null);
						m3=true;
						md.Update(m);
						System.out.println("Monstuo despues de pegarse");
						System.out.println(mon.toString());
					}
				}
				contador++;
			}
		}
		if (nom.contains("Escupidor_Veneno") && m.getEnergia() > 4) {
			int numero = (int) (Math.random() * p.getnJugadors());
			int contador = 0;
			Set<Monstre> a = p.getMonstres();
			for (Monstre mon : a) {
				if (!mon.getNomMonstre().contains(m.getNomMonstre())) {
					if (contador == numero) {
						System.out.println("Monstuo antes de pegarse");
						System.out.println(mon.toString());
						mon.setPuntsVictoria(mon.getPuntsVictoria() - 1);
						md.Update(mon);
						m.setMonstre_de_poder(null);
						m4=true;
						md.Update(m);
						System.out.println("Monstuo despues de pegarse");
						System.out.println(mon.toString());
					}
				}
				contador++;
			}
		}
	}

	public Monstre getMonstreJugador(Jugador jugadore) {
		Set<Monstre> monstresJugador = jugadore.getMonstres();
		for (Monstre mo : monstresJugador) {
			if (mo.getNomMonstre().contains("Alienoid") || mo.getNomMonstre().contains("MekaDracron")
					|| mo.getNomMonstre().contains("King") || mo.getNomMonstre().contains("Ciberkitty")
					|| mo.getNomMonstre().contains("Space_Penguin") || mo.getNomMonstre().contains("Gigazaur")
							&& p.getId_partida() == mo.getId_partida().getId_partida()) {
				return mo;

			}
		}
		return null;
	}
}
