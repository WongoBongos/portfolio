package MapeadoClases;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Jugador")
public class Jugador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_jugador", insertable = false, updatable = false)
	private int id_jugador;

	@Column(name = "nom")
	private String nom;

	@Column(name = "cognom")
	private String cognom;

	@OneToMany(mappedBy = "jugador")
	private Set<Monstre> monstres = new HashSet<Monstre>();

	public Jugador() {
		super();
	}

	@Override
	public String toString() {
		return "Jugador [id_jugador=" + id_jugador + ", nom=" + nom + ", cognom=" + cognom + ", monstres=" + monstres
				+ "]";
	}

	public Jugador(String nom, String cognom, Set<Monstre> monstres) {
		super();
		this.nom = nom;
		this.cognom = cognom;
		this.monstres = monstres;
	}

	public int getId_jugador() {
		return id_jugador;
	}

	public void setId_jugador(int id_jugador) {
		this.id_jugador = id_jugador;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public Set<Monstre> getMonstres() {
		return monstres;
	}

	public void setMonstres(Set<Monstre> monstres) {
		this.monstres = monstres;
	}

}
