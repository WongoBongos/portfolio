package MapeadoClases;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Monstre")
public class Monstre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_monstre", insertable=false, updatable = false)
	private int id_monstre;
	
	@Column(name = "nomMonstre")
	private String nomMonstre;
		
	@ManyToOne
	@JoinColumn(name = "idpartida")
	private Partida id_partida;
	
	@ManyToOne
	@JoinColumn(name = "id_jugador", nullable = true)
	private Jugador jugador;
	
	@Column(name = "vides", nullable = true)
	private int vides;
	
	@Column(name = "puntsVictoria", nullable = true)
	private int puntsVictoria;
	
	@Column(name = "energia", nullable = true)
	private int energia;
	
	@Column(name = "EstaATokyo", columnDefinition = "boolean default false")
	private boolean EstaAToquio;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "monstreDePoder", updatable = true)
	private Monstre MonstreAsociat;

	@OneToOne(mappedBy = "MonstreAsociat", cascade = CascadeType.REFRESH, fetch=FetchType.EAGER)
	private Monstre monstre_de_poder;

	@Override
	public String toString() {
		return "Monstre [id_monstre=" + id_monstre + ", nomMonstre=" + nomMonstre + ", vides=" + vides + ", puntsVictoria=" + puntsVictoria + ", energia="
				+ energia + ", EstaAToquio=" + EstaAToquio +  "]";
	}

	public Monstre() {
		super();
	}

	public int getId_monstre() {
		return id_monstre;
	}

	public void setId_monstre(int id_monstre) {
		this.id_monstre = id_monstre;
	}

	public String getNomMonstre() {
		return nomMonstre;
	}

	public void setNomMonstre(String nomMonstre) {
		this.nomMonstre = nomMonstre;
	}

	public Partida getId_partida() {
		return id_partida;
	}

	public void setId_partida(Partida id_partida) {
		this.id_partida = id_partida;
	}

	public Jugador getId_jugador() {
		return jugador;
	}

	public void setId_jugador(Jugador id_jugador) {
		this.jugador = id_jugador;
	}

	public int getVides() {
		return vides;
	}

	public void setVides(int vides) {
		this.vides = vides;
	}

	public int getPuntsVictoria() {
		return puntsVictoria;
	}

	public void setPuntsVictoria(int puntsVictoria) {
		this.puntsVictoria = puntsVictoria;
	}

	public int getEnergia() {
		return energia;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}

	public boolean isEstaAToquio() {
		return EstaAToquio;
	}

	public void setEstaAToquio(boolean estaAToquio) {
		EstaAToquio = estaAToquio;
	}

	public Monstre getMonstre_de_poder() {
		return MonstreAsociat;
	}

	public void setMonstre_de_poder(Monstre monstre_de_poder) {
		this.MonstreAsociat = monstre_de_poder;
	}
	
	
	public Monstre(Monstre monstre_de_poder) {
		super();
		this.MonstreAsociat = monstre_de_poder;
	}

	public Monstre(String nomMonstre, Partida id_partida, Jugador id_jugador, int vides, int puntsVictoria, int energia,
			boolean estaAToquio, Monstre monstre_de_poder) {
		super();
		this.nomMonstre = nomMonstre;
		this.id_partida = id_partida;
		this.jugador = id_jugador;
		this.vides = vides;
		this.puntsVictoria = puntsVictoria;
		this.energia = energia;
		EstaAToquio = estaAToquio;
		this.monstre_de_poder = monstre_de_poder;
	}
}
