import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import org.json.simple.JSONObject;

public class Handler implements Runnable {
	private Socket m_socket;
	private SocketInterface m_sif;
	private Server m_server;
	private int intentos = 5;

	public Handler(Socket m_socket, Server m_server) throws IOException {
		super();
		this.m_socket = m_socket;
		this.m_server = m_server;

		m_sif = new SocketInterface(m_socket);
		m_sif.verbose = true;
		m_sif.verbosePreamble = m_socket.getInetAddress() + " -> ";
	}

	private String m_nickname;

	private void Partida() throws CommunicationProtocolException, IOException {
		try {
			m_sif.sendAndReceive(Protocolo.HOTEL, Protocolo.ACK);
		} catch (CommunicationProtocolException | IOException e2) {
			e2.printStackTrace();
		}

		try {
			m_nickname = m_sif.receive();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (m_server.comprobarNick(m_nickname)) {
			do {
				m_sif.send(Protocolo.NICK_EN_US);
				try {
					m_nickname = m_sif.receive();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} while (m_server.comprobarNick(m_nickname));
		}

		m_sif.send(Protocolo.ACK);
		if (m_server.conectarse()) {
			synchronized (m_server.loby) {
				try {
					System.out.println("Esperando");
					m_server.loby.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try {
				m_sif.sendAndReceive(Protocolo.ESTAS_DINS, Protocolo.ACK);
				m_sif.sendAndReceive(Protocolo.TRIVAGO, Protocolo.ACK);
			} catch (CommunicationProtocolException | IOException e2) {
				e2.printStackTrace();
			}
			synchronized (m_server.gameStart) {
				m_server.playerReady();
				System.out.println("ToyReady");
				try {
					m_server.gameStart.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			intentos();
			synchronized (m_server.playersInGame) {
				m_server.playersInGame.put(m_nickname, intentos);
			}
			ArrayList<JSONObject> JSonArray = new ArrayList<JSONObject>();
			for (String nom : m_server.playersInGame.keySet()) {
				JSONObject json = new JSONObject();
				json.put("nick", nom);
				if (m_server.playersInGame.get(nom) > 5) {
					json.put("ok", "NO");
					json.put("Intents", 5);
				} else {
					json.put("ok", "SI");
					json.put("Intents", m_server.playersInGame.get(nom));
				}
				JSonArray.add(json);
			}
			JSONObject json = new JSONObject();
			json.put("resultats", JSonArray);
			m_sif.send(json.toJSONString());
			m_sif.send(Protocolo.ACASO_ERES_FRANCES);
			String Opcion;

			try {
				Opcion = m_sif.receive();
				switch (Opcion) {
				case Protocolo.CHORIZO_PORTUANO:
					Partida();
					break;
				case Protocolo.WI:
					m_sif.send(Protocolo.ACK);
					m_server.m_currentPlayersNicknames.remove(m_nickname);
					m_server.n_players--;
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			try {
				m_sif.sendAndReceive(Protocolo.EN_CURS, Protocolo.ACK);
			} catch (CommunicationProtocolException | IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Partida();
		} catch (CommunicationProtocolException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void intentos() throws IOException, CommunicationProtocolException {
		intentos = 0;
		while (intentos < 5) {
			intentos++;

			m_sif.sendAndReceive(Protocolo.ESCRIU_LA_PARAULA, Protocolo.ACK);

			String palabro = m_sif.receive();

			m_sif.send(Protocolo.ACK);
			palabro.toLowerCase();
			if (palabro.equals(m_server.palabro)) {
				m_sif.send(Protocolo.DIABLO);
				return;
			} else {
				String p = "";
				m_sif.send(Protocolo.PARLEM);
				for (int j = 0; j < m_server.palabro.length(); j++) {
					if (m_server.palabro.charAt(j) == palabro.charAt(j))
						p += "b";
					else
						p += "f";
				}
				
				JSONObject json = new JSONObject();
				json.put("Palabra", "" + p);
				m_sif.send(json.toJSONString());
		
			}
		}
		intentos++;
		m_sif.sendAndReceive(Protocolo.CAGASTE, Protocolo.ACK);
	}
}
