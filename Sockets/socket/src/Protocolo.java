public class Protocolo {
	
	public static final String HOTEL = "HOTEL";
	public static final String ACASO_ERES_FRANCES = "ACASO_ERES_FRANCES?";
	public static final String TRIVAGO = "TRIVAGO";
	public static final String ESCRIU_LA_PARAULA = "ESCRIU_LA_PARAULA";
	public static final String DIABLO = "DIABLO";
	public static final String PARLEM = "PARLEM";
	public static final String CAGASTE = "CAGASTE";
	public static final String ACK = "ACK";
	public static final String WI = "WI";
	public static final String CHORIZO_PORTUANO = "CHORIZO_PORTUANO";
	public static final String EN_CURS = "S_EN_CURS";
	public static final String ESTAS_DINS = "S_ESTAS_DINS";
	public static final String NICK_EN_US = "S_NICK_EN_US";
	
}
