import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

public class Server implements Runnable {

	public HashSet<String> m_currentPlayersNicknames = new HashSet<String>();
	public HashMap<String, Integer> playersInGame = new HashMap<String, Integer>();
	public Object gameStart = new Object();
	public Object loby = new Object();
	public Object playerOnQueue = new Object();
	public int n_players = 0;
	public int n_watingPlayers = 0;
	private boolean m_gameRunning;
	public String palabro = "mango";

	public static void main(String[] args) {

		ExecutorService executor = Executors.newCachedThreadPool();
		Server server = new Server();

		System.err.println("SERVER: Awaiting connection at port: " + 20001);

		try (ServerSocket serverSocket = new ServerSocket(20001);) {
			executor.execute(server);
			while (true) {
				Socket clientSocket = serverSocket.accept();
				Handler handler = new Handler(clientSocket, server);
				try {
					executor.execute(handler);
				} catch (RejectedExecutionException e) {
					System.err.println("ERROR CREATING THREAD");
					clientSocket.close();
				}
			}

		} catch (IOException e) {
			System.out.println(
					"Exception caught when trying to listen on port " + 20001 + " or listening for a connection");
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println("Some weirdy weirdo exception.");
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void run() {
		while (true) {
			System.err.println("SERVER -> LOBBY WAITING FOR PLAYERS");

			try {

				synchronized (playerOnQueue) {
					n_players = 0;
					n_watingPlayers = 0;
					m_gameRunning = false;
					playerOnQueue.wait();
				}

			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			System.err.println("SERVER -> LOBBY COUNTDOWN");

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			playersInGame.clear();
			m_gameRunning = true;

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			System.err.println("SERVER -> LOBBY COUNTDOWN FINISHED, WAITING FOR EVERYONE");
			empezarPartida();
			try {
				waitForCurrentPlayers();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

	}

	private void empezarPartida() {

		System.out.println("ACABANDO ESTO");
		synchronized (loby) {
			loby.notifyAll();
		}

	}

	private void waitForCurrentPlayers() throws InterruptedException {
		synchronized (gameStart) {
			while (n_watingPlayers > 0) {
				System.err.println(
						"SERVER WAITING: current players=" + n_players + " awaiting players=" + n_watingPlayers);
				System.out.println(n_watingPlayers);
				gameStart.wait();
			}

			gameStart.notifyAll();
		}
	}

	public boolean comprobarNick(String m_nickname) {
		// TODO Auto-generated method stub

		if (m_currentPlayersNicknames.contains(m_nickname))
			return true;
		m_currentPlayersNicknames.add(m_nickname);
		return false;

	}

	public synchronized void playerReady() {
		// TODO Auto-generated method stub
		System.out.println("Pienso luego existo");
		n_watingPlayers--;

		System.out.println("Quedan " + n_watingPlayers + " a la espera");
		if (n_watingPlayers == 0)
			gameStart.notifyAll();

	}

	public synchronized Boolean conectarse() {
		// TODO Auto-generated method stub
		synchronized (playerOnQueue) {
			if (m_gameRunning)
				return false;
			n_players++;
			n_watingPlayers++;
			playerOnQueue.notifyAll();

			return true;
		}
	}



}
