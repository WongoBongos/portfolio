
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketInterface {
	private PrintWriter m_out;
	private BufferedReader m_in;
	private DataOutputStream m_dout;
	private DataInputStream m_din;

	public boolean verbose = true;
	public String verbosePreamble = "";

	public SocketInterface(Socket socket) throws IOException {
		m_out = new PrintWriter(socket.getOutputStream(), true);
		m_in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		m_dout = new DataOutputStream(socket.getOutputStream());
		m_din = new DataInputStream(socket.getInputStream());
	}

	public void send(String send) {
		if (verbose)
			System.out.println(verbosePreamble + send);
		m_out.println(send);
	}

	public String receive() throws IOException {
		return m_in.readLine();
	}

	public String receive(String receive) throws CommunicationProtocolException, IOException {
		String input = m_in.readLine();
		if (input == null || !input.equals(receive)) {
			if (verbose)
				System.out.println(verbosePreamble + "Expected " + receive + " and got " + input);
			throw new CommunicationProtocolException(verbosePreamble + "Expected " + receive + " and got " + input);
		}
		return input;
	}

	public String sendAndReceive(String send, String receive) throws CommunicationProtocolException, IOException {
		send(send);
		return receive(receive);
	}

	public String receiveAndSend(String receive, String send) throws CommunicationProtocolException, IOException {
		String answer = receive(receive);
		send(send);
		return answer;
	}

	public String sendAndReceive(String send) throws CommunicationProtocolException, IOException {
		send(send);
		return receive();
	}

	public String receiveAndSend(String send) throws CommunicationProtocolException, IOException {
		String answer = receive();
		send(send);
		return answer;
	}

	public void sendInt(int send) throws IOException {
		if (verbose)
			System.out.println(verbosePreamble + send);
		m_dout.writeInt(send);
	}

	public void sendByte(byte send) throws IOException {
		/*if(verbose) System.out.println(verbosePreamble +
				WordleMessages.msgToString(send));*/
		m_dout.writeByte(send);
	}

	public int receiveInt() throws IOException {
		int msg = m_din.readInt();
		if (verbose)
			System.out.println(verbosePreamble + "Received: " + msg);
		return msg;
	}

	public byte receiveByte() throws IOException {
		byte msg = m_din.readByte();
		/*if(verbose) System.out.println(verbosePreamble + "Received: " +
				PenjatMessages.msgToString(msg));*/
		return msg;
	}

	public byte receiveByte(byte receive) throws CommunicationProtocolException, IOException {
		byte input = m_din.readByte();
		/*if (input != receive) {
			if(verbose) System.out.println(verbosePreamble + "Expected "+
					WordleMessages.msgToString(receive) + " and got " +
					WordleMessages.msgToString(input));
				throw new CommunicationProtocolException(verbosePreamble + "Expected "+
						WordleMessages.msgToString(receive) + " and got " +
						WordleMessages.msgToString(input));
		}*/
		return input;
	}

	public byte sendAndReceive(byte send, byte receive) throws CommunicationProtocolException, IOException {
		sendByte(send);
		return receiveByte(receive);
	}

	public byte ReceiveAndSend(byte receive, byte send) throws CommunicationProtocolException, IOException {
		byte answer = receiveByte(receive);
		sendByte(send);
		return answer;
	}

	public byte sendAndReceive(byte send) throws CommunicationProtocolException, IOException {
		sendByte(send);
		return receiveByte();
	}

	public byte ReceiveAndSend(byte send) throws CommunicationProtocolException, IOException {
		byte answer = receiveByte();
		sendByte(send);
		return answer;
	}

	public void close() throws IOException {
		m_in.close();
		m_out.close();
		m_din.close();
		m_dout.close();
	}
}
