
public class CommunicationProtocolException extends Exception {
	private static final long serialVersionUID = 1L;

	public CommunicationProtocolException(String msg) {
		super(msg);
	}
}
