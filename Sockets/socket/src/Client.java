import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Client {
	public int intentos;
	public static void main(String[] args) throws UnknownHostException, IOException, CommunicationProtocolException {
		SocketInterface sif = null;
		String hostName = "localhost";
		int portNumber = 20001;

		
		Scanner sc = new Scanner(System.in);

		Socket socket = new Socket(hostName, portNumber);
		BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
		sif = new SocketInterface(socket);
		sif.verbose = true;
		Partida(sif, sc);

	}

	private static void Partida(SocketInterface sif, Scanner sc) throws CommunicationProtocolException, IOException {
	
		System.out.println("Hola");
		sif.receiveAndSend(Protocolo.HOTEL, Protocolo.ACK);

		String nkname = "";
		String out = "";
		do {
			System.out.println("Escribe tu nickname");
			nkname = sc.nextLine();
			out = sif.sendAndReceive(nkname);
		} while (out.equals(Protocolo.NICK_EN_US));
		
		
		if (!out.equals(Protocolo.ACK))
			throw new CommunicationProtocolException("Expected ACK and got " + out);
		do {
			System.out.println("Partida En Curs");
			out = sif.receiveAndSend(Protocolo.ACK);
		} while (out.equals(Protocolo.EN_CURS));
		
		System.out.println("Estas dentro");
		
		if (!out.equals(Protocolo.ESTAS_DINS))
			throw new CommunicationProtocolException("Expected ESTAS_DINS and got " + out);
		sif.receiveAndSend(Protocolo.TRIVAGO, Protocolo.ACK);
		
		while (true) {
			String a = sif.receive();
			if (a.equals(Protocolo.CAGASTE)) {
				sif.send(Protocolo.ACK);
				break;
			}
			sif.send(Protocolo.ACK);
			String palabro = "";
			if (a.equals(Protocolo.ESCRIU_LA_PARAULA)) {
			
				while (true) {
					System.out.println("Escribe algo:");
					palabro = sc.nextLine();
					if (palabro.length() == 5)
						break;
					System.out.println("La palabra debe tener 5 letras");
				}
			}else {
				throw new CommunicationProtocolException("El text del buffer no coincideix amb el demanat");
			}
			
				sif.send(palabro);
				sif.receive(Protocolo.ACK);
				String b = sif.receive();
				System.out.println(b);
				if (b.equals(Protocolo.DIABLO)) {
					System.out.println("WIN");
					break;
				}
				
				if (!b.equals(Protocolo.PARLEM))
					throw new CommunicationProtocolException("El text del buffer no coincideix amb el demanat");
				String JsonString = sif.receive();
				System.out.println(JsonString);
			
		}
		FinalPartida(sif, sc);
	}

	public static void FinalPartida(SocketInterface sif, Scanner sc) throws CommunicationProtocolException, IOException {
		String respuesta = sif.receive();
		System.out.println(respuesta);
		sif.receive(Protocolo.ACASO_ERES_FRANCES);
		
		System.out.println("Pulsa 1: Seguir Jugando");
		System.out.println("Pulsa 2: Frances?");
		
		String eleccion = sc.next();
		if (eleccion.equals("1")) {
			sif.send(Protocolo.CHORIZO_PORTUANO);
			Partida(sif, sc);
		} else if (eleccion.equals("2")) {
			sif.send(Protocolo.WI);
		} else {
			System.out.println("1 o 2 nada m�s");
			FinalPartida(sif, sc);
		}

	}

}
