package Buscaminas;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Buscaminas {
	static Scanner sc = new Scanner(System.in);
	static int numMinas = 5;
	static int FILAS = 10;
	static int COLUMNAS = 10;
	static String Nombre;
	static Board b = new Board();
	static Window w = new Window(b);
	static ArrayList<String> Winner = new ArrayList<String>();

	public static void main(String[] args) throws InterruptedException {
		menu();
	
	}
/**
 * El men� principal que et deixa jugar , mostar les regles, altres opcions o salir del joc
 * @throws InterruptedException lo que et deixa jugar amb ratol�
 */
	private static void menu() throws InterruptedException {

		boolean sortir = false;
		while (!sortir) {
			Options();
			int a = sc.nextInt();
			switch (a) {
			case 1:
				mostrarAyuda();
				break;

			case 2:
				opcions();
				break;

			case 3:
				Jugar();
				break;

			case 4:
				Winner(Nombre);
				break;

			case 0:
				sortir = true;
				break;

			}
		}

	}
/**
 * El renking dels que han guanyat el joc
 * @param nombre2 el nombre que introdueixes a el ranking
 */
	private static void Winner(String nombre2) {
		System.out.println(Winner);
	}
/**
 * La funci� que est deixa jugar al joc, primer initzialitzant el taulell,
 * el taulell de minas, i el GUI. Despr�s ve el bucle que mira si esta en curs la patida,
 * on estan funcions per veure el camp, descubrir les mines del camp i per �ltim la
 * funci� que determina si has perdut o guanyat.
 * @throws InterruptedException Lo que et deixa jugar amb ratol�
 */
	private static void Jugar() throws InterruptedException {

		int[][] campo = inserirtaulell();
		int[][] minas = inserirMinas();
		inicialitzarGUI();
		boolean PartidaEnCurs = true;
		while (PartidaEnCurs) {
			Vercampo(campo);
			Posicio p = ElegirPosicion(campo);
			campo = descubrir(p.fila, p.columna, campo, minas);
			int DD = minas[p.fila][p.columna];
			PartidaEnCurs = partidaAcabada(DD, campo, minas);

		}

	}
	/**
	 * Posa un nombre a la posici� on has clicat que es refereix al nombre de minas que tens al voltan.
	 * En cas de que aquest contador sigui zero, tornara ha mirar les posicions alrededor teu que estiguin lliures
	 * de mines fins que trobi una.
	 * @param fila la fila on has clicat
	 * @param columna la columna on has clicat
	 * @param campo el taulell del joc
	 * @param minas el taulell de minas
	 * @return el taulell del joc destapa
	 */

	private static int[][] descubrir(int fila, int columna, int[][] campo, int[][] minas) {
		if (minas[fila][columna] == 8) {
			campo[fila][columna] = -1;
			return campo;
		}

		int n = destapar(fila, columna, minas);
		campo[fila][columna] = n;
		if (campo[fila][columna] == 0) {
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					if (fila + i >= 0 && fila + i < minas.length && columna + j >= 0 && columna + j < minas[0].length) {
						if (campo[fila + i][columna + j] == 9) {
							int filas = fila + i;
							int columnas = columna + j;
							descubrir(filas, columnas, campo, minas);
						}
					}
				}
			}
		}
		return campo;
	}
/**
 * Destapa les 8 posicions al voltant de la posici� on has clicat per mirar si hi ha minas i les conta en cas de
 * que hi hagi
 * @param fila fila d'on has clicat
 * @param columna columna d'on has clicat
 * @param minas el taulell de minas
 * @return el contador de minas
 */
	private static int destapar(int fila, int columna, int[][] minas) {
		int contador = 0;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (fila + i >= 0 && fila + i < minas.length && columna + j >= 0 && columna + j < minas[0].length) {
					if (minas[fila + i][columna + j] == 8) {
						contador++;
					}
				}
			}
		}

		return contador;
	}
/**
 * Comprova si has guanyat o si has perdut la partida
 * @param DD Comprovador de si estas en una mina
 * @param campo el taulell del joc
 * @param minas el taulell de minas
 * @return boolea que determina si has guanyat
 */
	private static boolean partidaAcabada(int DD, int[][] campo, int[][] minas) {
		boolean PERDEDER = true;
		if (DD == 8) {
			System.out.println("Moriste");
			for (int i = 0; i < minas.length; i++) {
				for (int j = 0; j < minas[0].length; j++) {
					System.out.print(minas[i][j] + " ");
				}
				System.out.println();
			}
			PERDEDER = false;
		}
		if (CasillasLibres(campo) == numMinas) {
			System.out.println("HAS GANADO WOOOHOO!!!!");
			Winner.add(Nombre);
			for (int i = 0; i < minas.length; i++) {
				for (int j = 0; j < minas[0].length; j++) {
					System.out.print(minas[i][j] + " ");
				}
				System.out.println();
			}
			PERDEDER = false;
		}
		return PERDEDER;
	}
/**
 * Verifica que hi hagi caselles lliures en el taulell i les conta.
 * @param campo el taulell del joc
 * @return les casillas lliures en forma de contador.
 */
	private static int CasillasLibres(int[][] campo) {
		int cont = 0;
		for (int i = 0; i < campo.length; i++) {
			for (int j = 0; j < campo[0].length; j++) {
				if (campo[i][j] == 9) {
					cont++;
				}
			}
		}
		return cont;
	}

	/**
	 * Imprimeix el taulell per pantalla
	 * 
	 * @param campo el taulell del joc
	 */

	private static void Vercampo(int[][] campo) {
		for (int i = 0; i < campo.length; i++) {
			for (int j = 0; j < campo[0].length; j++) {
				System.out.print(campo[i][j] + " ");
			}
			System.out.println();
			b.draw( campo, 't');
		}
	}

	/**
	 * Comprova si la posici� que has elegit es valida
	 * 
	 * @param campo el taulell del joc
	 * @return true si la posici� es valida i false si no ho es
	 * @throws InterruptedException 
	 */
	private static Posicio ElegirPosicion(int[][] campo) throws InterruptedException {
		w.setActLabels(false);
		w.setDebugLabel(true);
		Posicio p = new Posicio();
		p.fila = -1;
		p.columna = -1;
	while(p.fila==-1) {
		p.fila = b.getCurrentMouseRow();
		p.columna = b.getCurrentMouseCol();
		Thread.sleep(50);
	
	}
	return p;
	}

	/**
	 * Omple el taullel de minas amb 0, per despres posar en posicions aleatorias 8
	 * que representan las minas
	 * 
	 * @return el taulell de minas amb les mines colocades
	 */
	private static int[][] inserirMinas() {
		int[][] mat = new int[FILAS][COLUMNAS];
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat.length; j++) {
				mat[i][j] = 0;
			}
		}
		int mina = 8;
		Random rd = new Random();
		int minasDeEstaPartida = numMinas;
		while (minasDeEstaPartida > 0) {
			int a = rd.nextInt(mat.length);
			int b = rd.nextInt(mat[0].length);
			if (mat[a][b] == 0) {
				mat[a][b] = mina;
				minasDeEstaPartida--;
			}
		}
		return mat;

	}

	/**
	 * Crea el taullel en el que jugarem
	 * 
	 * @return la matriu que far� de taulell plena de 9
	 */

	private static int[][] inserirtaulell() {
		int[][] mati = new int[FILAS][COLUMNAS];
		for (int i = 0; i < mati.length; i++) {
			for (int j = 0; j < mati.length; j++) {
				mati[i][j] = 9;
			}
		}
		return mati;
	}

	/**
	 * El men� d'opcions que ens permetra fer 4 coses, posar el nostre nom, elegir
	 * el tamany del taulell, el nombre de minas i salir del men�
	 */
	private static void opcions() {
		boolean exit = false;
		while (!exit) {
			OpcionesDentroDeOpcionesDentroDeOciones();
			int a = sc.nextInt();
			switch (a) {
			case 1:
				System.out.println("Pon tu nombre");
				Nombre = sc.next();
				break;

			case 2:
				System.out.println("Inserta el numero de filas y columnas que quieres que tu tablero tenga");
				ElegirTama�oTabla();
				break;

			case 3:
				System.out.println(
						"Inserta el numero de explosivos desmembradores matadores aniquiladores masacradores de personas que quieres que tu tablero tenga");
				ElegirNumMinas();
				break;

			case 4:
				exit = true;
				break;
			}
		}

	}

	/**
	 * Elegiex el nombre de minas amb las que jugar�s
	 */
	static void ElegirNumMinas() {
		numMinas = sc.nextInt();
	}

	/**
	 * Eligeix el tamany del taulell, que tamb� afecta al taulell de minas
	 * 
	 * @return el taulell amb el tamany elegit
	 */
	static int[][] ElegirTama�oTabla() {
		FILAS = sc.nextInt();
		COLUMNAS = sc.nextInt();
		int[][] mat = new int[FILAS][COLUMNAS];

		return mat;
	}
/**
 * Mostra les opcions dintre de la opci� de opcions del men� principal
 */
	private static void OpcionesDentroDeOpcionesDentroDeOciones() {
		System.out.println("1.- Poner nombre\r\n" + "2.- Elige el tama�o del tablero\r\n"
				+ "3.- Elige el n�mero de minas\r\n" + "4.- Salir\r\n");

	}
/**
 * Mostra les regles del joc
 */
	private static void mostrarAyuda() {
		System.out.println(
				"El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.\r\n" + "\r\n"
						+ "Algunas casillas tienen un n�mero, este n�mero indica las minas que son en todas las casillas circundantes. As�, si una casilla tiene el n�mero 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o borde) hay 3 con minas y 5 sin minas. Si se descubre una casilla sin n�mero indica que ninguna de las casillas vecinas tiene mina y estas se descubren autom�ticamente.\r\n"
						+ "\r\n" + "Si se descubre una casilla con una mina se pierde la partida.\r\n" + "\r\n"
						+ "Se puede poner una marca en las casillas que el jugador piensa que hay minas para ayudar a descubrir la que est�n ");
	}
/**
 * Mostra les opcions del men� principal
 */
	private static void Options() {
		System.out.println("1.- Mostrar Ayuda\r\n" + "2.- Opciones\r\n" + "3.- Jugar Partida\r\n"
				+ "4.- Ver Rankings\r\n" + "0.- Salir\r\n" + "");

	}
	/**
	 * L'interfa� gr�fica que permet al jugador poder jugar el joc amb colors i estil.
	 */
	private static void inicialitzarGUI() {
		
		b.setColorbackground(0xb1adad); //posa el color de fons del background
		//el color est� escrit en hexa RBG, �s a dir, �s un nombre hexadecimal que cada parella de 2 bits, de 00 a FF, representa el nombre RBG de 0 a 255. Per tant, 0xFF000 �s vermell, 0x00FF00 �s verd, i 0x0000FF �s blau
		b.setActborder(true);  //fa que siguin visibles les vores entre caselles
		String[] lletres = { "", "1", "2", "3", "4", "5", "6", "7", "8", "*" };  //qu� s'ha d'escriure en cada casella en base al nombre. Per tant, per exemple, si el nombre de la matriu �s 0 no s�escriur� res, si �s 1 s�escriur� la string �1�, i si es 9 s�escriur� la String �*�
		b.setText(lletres);
		int[] colorlletres = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x521b98, 0xFFFFFF, 0xFF8000, 0x7F00FF }; //el color de les lletres, que veient com funcionava la String de lletres ja entendreu com va
		b.setColortext(colorlletres);
		String[] etiquetes2 = { "Mines: 10"};  //les etiquetes que es mostraran a la dreta de la pantalla. Haureu de canviar el camp de mines per la vostra variable
		w.setLabels(etiquetes2);
		w.setActLabels(true);
		w.setTitle("Cercamines");
	
		}

}

